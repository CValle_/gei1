\documentclass[a4paper, 12pt, titlepage]{extarticle}
\usepackage[utf8]{inputenc}
\usepackage[spanish]{babel}
\usepackage{fancyhdr}

\setlength{\parindent}{0pt} % no indent paragraphs
\usepackage[margin=1in,bottom=1.2in,top=1.4in]{geometry}

\usepackage{amsmath, mathtools, amssymb, amsfonts, mathrsfs} %math
\usepackage{array}
\usepackage{multicol}
\usepackage{pifont} %symbols font

\usepackage{enumitem}
\usepackage{hyperref} %links
\usepackage{spalign} %systems
\spalignsysdelims{.}{\}} %system bracket on the right

% -- matrix
\makeatletter
\renewcommand*\env@matrix[1][*\c@MaxMatrixCols c]{%
  \hskip -\arraycolsep
  \let\@ifnextchar\new@ifnextchar
  \array{#1}}
\makeatother

% -- colors
\usepackage{soul}
\usepackage[dvipsnames]{xcolor}
\hypersetup{
    colorlinks,
    linkcolor={black},
    citecolor={blue!50!black},
    urlcolor={blue!80!black}
}

\definecolor{Lilac}{HTML}{c3cde6}

\newcommand{\hllc}[1]{\colorbox[HTML]{c3cde6}{$\displaystyle #1$}} % formato ancho cian
\newcommand{\hllr}[1]{\colorbox[HTML]{ffd7c6}{$\displaystyle #1$}} % formato ancho rosa
\newcommand{\hlfancy}[2]{\sethlcolor{#1}\hl{#2}} % no afecta al ancho del caracter

\usepackage{tikz}
\usepackage[siunitx]{circuitikz}

\newcommand{\dsquare}{$\square \text{  }$}
\newcommand{\minisection}[2]{\dsquare\textbf{#1}\text{  #2}}
\newcommand{\enter}{\vspace{20px}}
\newcommand{\congr}{$\mathbb{Z}_m$}
\newcommand{\xmark}{\ding{55}}

%--- MAKETITLE ------------------------------------------------------------------------------

\def\myauthor{Clara Valle Gómez}
\author{\myauthor}

\title{
    \begin{Huge}
        {\textbf{ÁLGEBRA}}\\
    \end{Huge}
    \vspace{50px}
    \textbf{Tema II}\\
    Sistemas de ecuaciones, matrices y determinantes
    \date{30 de marzo de 2022} %\today
}

%--- CABECERA Y PIE DE PÁGINA -------------------------------------------------------------------------
%\setlength{\headheight}{16.00pt} %warning
\pagestyle{fancy}
\lhead{Tema 2}
\chead{}
\rhead{C. Valle} 
\lfoot{}
\cfoot{}
\rfoot{\thepage}    

%--- DOCUMENTO -------------------------------------------------------------------------------

\begin{document}
    \begin{figure}
        \centering
        %\vspace*{4cm}
        %\includegraphics[width=230px]{media/logo.png}
        %\caption{logo}
    \end{figure}
    \maketitle

    
%--- ÍNDICE -------------------------------------------------------------------------
    \newpage
    \tableofcontents

%--- DOC ----------------------------------------------------------------------------   
    \newpage
    \section{Introducción a los sistemas de ecuaciones lineales}
    \minisection{Ecuaciones lineales}{}\\

    Llamamos ecuación lineal con las variables ($x_1,...,x_n$) y con coeficientes en $\mathbb{K}$ a toda expresión de la forma
    \[a_1x_1+...a_nx_n=b\]
    Una solución de la ecuación lineal es una n-tupla $s_1,...,s_n$ de valores de las variables que satisfacen la ecuación
    \[a_1s_1 + ... + a_ns_n=b\]

    \minisection{Sistemas de ecuaciones}{}\\
    
    Un sistema de m ecuaciones lineales con n incógnitas es una colección de m ecuaciones lineales emn las variables $x_1,...,x_n$ 

    \[
        \spalignsys {
            a_{11}x_1 \ +  a_{12}x_2 \  + a_{13}x_n \  = b_1 \  ;
            a_{21}x_1 \ +  a_{22}x_2 \  + a_{23}x_n \  = b_2 \  ;
            a_{m1}x_1 \ +  a_{m2}x_2 \  + a_{m3}x_n \  = b_m \  ;
        }
    \]
    \\
    Llamaremos $a_{ij}$ al coeficiente de la incógnita y $b_i$ a los términos independientes.\\

    Existen distintos tipos de sistemas dependiendo de las soluciones que admiten:

    \begin{itemize}
        \item \textbf{Sistemas compatibles (SC)}: tiene alguna solución
        \begin{itemize}
            \item \textbf{Compatible determinado (SCD)}: tiene una única solución
            \item \textbf{Compatible indeterminado (SCI)}: tiene infinitas soluciones
        \end{itemize}
        \item \textbf{Sistemas incompatibles (SI)}: no tiene solución
    \end{itemize}

    Un \textbf{sistema homogéneo} es uno en el que los términos independientes son todos nulos.

    \section{Matriz ampliada asociada}
    Cogiendo los coeficientes del sistema, obtenemos su matriz apliada asociada:
    \[
        \begin{pmatrix}[ccc|c]
            a_{11} & a_{12} & a_{1n} & b_1\\
            a_{21} & a_{22} & a_{2n} & b_2\\
            a_{m1} & a_{m2} & a_{mn} & b_m\\
        \end{pmatrix}
    \]
    
    Para resolver un sistema de ecuaciones se le puede aplicar a cada ecuación las siguientes \textbf{operaciones elementales}:
    \begin{itemize}
        \item permutar dos ecuaciones del sistema
        \item multiplicar una ecuación del sistema por una constante no nula
        \item sumar un múltiplo de una ecuación del sistema a otra ecuación
    \end{itemize}

    \section{Eliminación gaussiana}
    La eliminación gaussiana consiste en transformar la matriz ampliada del sistema en una más sencilla llamada escalonada (reducida) a través de operaciones elementales.\\

    \hlfancy{Lilac}{Una \textbf{matriz escalonada} verifica las siguientes condiciones:}
    \begin{itemize}
        \item Las filas con todos los elementos nulos se reagrupan en la parte baja de la matriz
        \item En toda fila no nula el primer elemento no nulo vale 1 (\textbf{pivote})
        \item En dos filas sucesivas con elementos no nulos el pivote de la fila inferior se encuentra a la derecha del pivote de la fila superior
    \end{itemize}

    Una matriz es \textbf{escalonada reducida} si es escalonada y además toda columna teniendo un pivote tiene nulos todos los demás elementos.\\

    \[
        \begin{pmatrix}[ccc|c]
            \mathbf{1} & 0 & 0 & 2\\
            0 & 0 & \mathbf{1} & 1\\
            0 & 0 & 0 & 0\\
        \end{pmatrix}
        \hspace{80px}
        \begin{pmatrix}[ccc|c]
            \textbf{1} & 0 & \color{Red}1 & 0\\
            0 & 0 & \mathbf{1} & 1\\
            0 & 0 & 0 & 0\\
        \end{pmatrix}
    \]

    \hspace{70px} Matriz escalonada reducida \hspace{35px} Matriz escalonada

    \subsection{Estudio y resolución de un sistema de ecuaciones lineales}
    Consideramos un sistema de $m$ ecuaciones y $n$ incógnitas y al transformar la matriz ampliada en \hlfancy{Lilac}{una matriz en escalonada con $r$ pivotes}:
    \begin{itemize}
        \item \textbf{El sistema es incompatible} si la columna de términos independientes tiene algún pivote.
        \item \textbf{El sistema es compatible} en otro caso, pudiendo ser:
        \begin{itemize}
            \item Si $r=n$, el sistema es determinado (tantos pivotes como incógnitas)
            \item Si $r<n$, el sistema es indeterminado (menos pivotes que incógnitas)
        \end{itemize}
    \end{itemize}

    \hlfancy{Lilac}{En un sistema de ecuaciones escalonado, las variables con pivote son \textbf{variables directoras o ligadas} y las que no son \textbf{variables libres}}
    \hspace{10px}
    \[
        \hspace{45px} 
        x_1 \ x_2 \ x_3 \ x_4 \ x_5
    \]
    \[
        Ejemplo:
        \begin{pmatrix} [ccccc|c]
            \mathbf{1} & 6 & 2 & 6 & 9 & 7 \\
            0 & 0 & \mathbf{1} & 3 & 1 & 3 \\
            0 & 0 & 0 & 0 & \mathbf{1} & 7
        \end{pmatrix}
    \]
    \begin{center}
        Es escalonada: es compatible. Más incognitas que ecuaciones: S.C.I\\
        Variables ligadas: ($x_1, x_3, x_5$). Variables libres: ($x_2, x_4$)
    \end{center}
    

    \section{Operaciones con matrices}
    Una matriz $A=a_{ij}$ de orden $m\times n$ tiene $m$ filas y $n$ columnas.\\

    Los coeficientes $a_{11},a_{22},..., a_{nn}$ son los elementos diagonales. La matriz cuadrada de orden $n$ cuyos elementos diagonales son uno y el resto nulos, es la \textbf{matriz identidad}.\\

    \minisection{Suma de matrices}{}\\

    Sean $A=a_{ij}$ y $B=b_{ij}$ matrices de orden $m\times n$ (del mismo tamaño), se suman uno a uno los coeficientes.

    \[
        \text{En } \mathbb{Z}_5 \hspace{10px}
        A = 
        \begin{pmatrix}
            3 & 1\\
            1 & 2\\
        \end{pmatrix}
        \hspace{10px}
        B =
        \begin{pmatrix}
            0 & 1\\
            2 & 4\\
        \end{pmatrix}
        \hspace{10px}
        A + B =
        \begin{pmatrix}
            3 & 2\\
            3 & 1\\
        \end{pmatrix}
    \]

    Verifica las siguientes propiedades:
    \begin{itemize}
        \item \textbf{conmutativa}: $A+B=B+A$
        \item \textbf{asociativa}: $a+(B+C)=(A+B)+C$
        \item \textbf{elemento neutro}: La matriz 0 de orden $n\times m$ verifica: $A+0=A$
        \item \textbf{elemento opuesto} La matriz $-A=(-a_{ij})$ verifica: $A+(-A)=0$
    \end{itemize}
    \minisection{Producto de matriz por un escalar}{}\\

    Sea $A=a_{ij}$ matriz de orden $m\times n$ con coeficientes en $\mathbb{K}$, k es un escalar. El producto se denomina $kA = ka_{aj}$\\

    \minisection{Producto de matrices}{}\\
    Sea una matriz $A=(a_{ij} \ m \times n)$ y $B=(b_{ij} \ n \times r)$, la matriz C se calcula sumando los productos de las filas de una por las columnas de otra

    \[
        \text{En } \mathbb{Z}_7 \hspace{10px}
        A = 
        \begin{pmatrix}
            4 & 1 & 5\\
            3 & 2 & 0\\
        \end{pmatrix}
        \hspace{10px}
        B =
        \begin{pmatrix}
            3 & 2\\
            4 & 0\\
            2 & 3\\
        \end{pmatrix}
    \]

    \enter
    \begin{tikzpicture}
        \hspace{50px}
        $
            C =
            \begin{pmatrix}
                5 & 6\\
                3 & 2\\
            \end{pmatrix}
        $
        \hspace{10px}
        \begin{scope}[]
            $\begin{aligned} 
                c_{11}= 4 \cdot 3+1 \cdot 4+5 \cdot 2=5 \hspace{10px} c_{12}= 4 \cdot 2+1 \cdot 0+ 5\cdot 3 = 2\\
                c_{21}= 3 \cdot 3+2 \cdot 4+0 \cdot 2=3 \hspace{10px} c_{22}= 3 \cdot 2+2 \cdot 0+3 \cdot 0 = 6
            \end{aligned}$
        \end{scope}
    \end{tikzpicture}\\

    Verifica las siguientes propiedades:
    \begin{itemize}
        \item no es conmutativa
        \item \textbf{asociativa}: $A(BC)=(AB)C$
        \item \textbf{distributiva} del producto respecto a la suma: $A(B+B') = AB+AB'$
        \item $(kA)B=A(kB)=k(AB)$
    \end{itemize}

    \section{La trasposición}
    La matriz traspuesta de $A$, denotada por $A^t$, es la matriz de orden $n \times m$ donde la $i$ fila de $A$ se transforma en la i columna de $A$, es decir, se intercambian filas por columnas.
    \[(A^t)_{ji} = a_{ij}\]

    Verifica las siguientes propiedades:
    \begin{multicols}{2}
        \begin{itemize}
            \item $(A^t)^t = A$
            \item $(A+B)^t=A^t+B^t$
            \item $(kA)^t = k\cdot A^t$
            \item $(AB)^t = B^tA^t$
        \end{itemize}
    \end{multicols}

    \subsection{Propiedades según la forma de la matriz}
    \begin{itemize}
        \item \textbf{simétrica}: Si es igual a su traspuesta $A=A^t$ o $a_{ij}=a_{ji}$
        \item \textbf{antisimétrica}: si es la opuesta de la traspuesta $A=-A^t$ o $-a_{ij}$
        \item \textbf{triangular superior}: si $a_{ij}=0$ para todo $i>j$
        \item \textbf{triangular inferior}: si $a_{ij}=0$ para todo $i<j$
        \item \textbf{diagonal}: si $a_{ij}=0$ para todo $i\not= j$ (es a la vez triangular superior e inferior)
    \end{itemize}

    \section{Forma matricial de un sistema de ecuaciones}
    Un sistema de ecuaciones se puede escribir en forma matricial $A\cdot X = B$, donde $A$ es la matriz de coeficientes, $X$ la matriz de incógnitas y $B$ la matriz de términos independientes.
    \[
        \begin{pmatrix}
            a_{11} & a_{12} & a_{1n}\\
            a_{21} & a_{22} & a_{2n}\\
            a_{m1} & a_{m2} & a_{mn}\\
        \end{pmatrix}
        \begin{pmatrix}
            x_1\\
            x_2\\
            x_n\\
        \end{pmatrix}
        =
        \begin{pmatrix}
            b_1\\
            b_2\\
            b_m\\
        \end{pmatrix}
    \]

    \section{Matrices elementales}
    Se llama matriz elemental $E$ de orden $n$ a la matriz resultante de aplicar solo una operación elemental por filas a la matriz identidad de orden $n$.
    Existen tres tipos de matrices elementales:
    \begin{itemize}
        \item $E_{ij}$ obtenida de intercambiar las filas $i$-ésima y $j$-ésima
        \item $E_i(k)$ obtenida de multiplicar por k los elementos de la fila $i$-ésima
        \item $E_{i+(k)j}$ obtenida de sumar a la fila $i$-ésima, la fila $j$-esima por $k$
    \end{itemize}
    \[
        E_{ij} = 
        \begin{pmatrix}
            1 & "" & "" & ""\\
            "" & 0 & 1 & ""\\
            "" & 1 & 0 & ""\\
            "" & "" & "" & 1\\
        \end{pmatrix}
        \hspace{10px}
        E_{i}(k) = 
        \begin{pmatrix}
            1 & "" & "" \\
            "" & k & "" \\
            "" & "" & 1 \\
        \end{pmatrix}
        \hspace{10px}
        E_{i+(k)j}=
        \begin{pmatrix}
            1 & "" & "" & ""\\
            "" & 1 & k & ""\\
            "" & "" & 1 & ""\\
            "" & "" & "" & 1\\
        \end{pmatrix}
    \]

    \section{Inversa de una matriz}
    Decimos que existe una matriz B inversa de A cuando:
    \[AB=I_n=BA\]
    La inversa de la matriz $A$ se denota como $A^{-1}$\\
    \[AA^{-1}=A^{-1}A=I_n\]

    Sólo tienen inversa las matrices cuadradas, y no todas las matrices cuadradas tienen inversa:
    \begin{itemize}
        \item Solo es invertible si se puede escribir en forma de producto de matrices elementales
        \item Cuando el determinante de una matriz es 0, no tiene inversa.
    \end{itemize}

    \subsection{Cálculo de la matriz inversa (Gauss)}
    Para calcular una matriz inversa se coloca la identidad a la derecha y se intenta conseguir esta en la izquierda. Así la que quede a la derecha será la matriz inversa.\\

    Ejercicio 6 i): \footnote{\quad \color{Periwinkle}Conseguir ceros \qquad \color{Peach}Conseguir unos}
    \[
        \text{en } \mathbb{Z}_5 \
        \begin{pmatrix}[ccc|ccc]
            1 & 4 & 1 & 1 & 0 & 0 \\
            \hllc{2} & 1 & 2 & 0 & 1 & 0 \\
            \hllc{3} & 0 & 3 & 0 & 0 & 1
        \end{pmatrix}
        \xrightarrow[\ F_3+2F_1\ ]{\ F_2+3F_1\ }
        \begin{pmatrix}[ccc|ccc]
            1 & 4 & 1 & 1 & 0 & 0 \\
            0 & 3 & 0 & 3 & 1 & 0 \\
            0 & \hllc{3} & 0 & 2 & 0 & 1
        \end{pmatrix}
        \xrightarrow{\ F_3-F_2\ }
        \begin{pmatrix}[ccc|ccc]
            1 & 4 & 1 & 1 & 0 & 0 \\
            0 & 3 & 0 & 3 & 1 & 0 \\
            0 & 0 & 0 & \hllr{4} & 4 & 1
        \end{pmatrix}
    \]
    \setulcolor{Peach}
    Acabaría teniendo un \ul{pivote} en el término independiente, por lo que no tiene inversa\\

    Ejercicio 6 ii):
    \[
        \text{en } \mathbb{Z}_5 \
        \begin{pmatrix}[ccc|ccc]
            2 & 0 & 4 & 1 & 0 & 0 \\
            0 & \hllr{3} & 0 & 0 & 1 & 0 \\
            \hllc{2} & 1 & 3 & 0 & 0 & 1
        \end{pmatrix}
        \xrightarrow[\ F_3-F_1\ ]{\ 2F_2\ }
        \begin{pmatrix}[ccc|ccc]
            \hllr{2} & 0 & 4 & 1 & 0 & 0 \\
            0 & 1 & 0 & 0 & 2 & 0 \\
            0 & \hllc{1} & 4 & 4 & 0 & 1
        \end{pmatrix}
        \xrightarrow[\ F_3-F_2\ ]{\ 3F_1\ }
        \begin{pmatrix}[ccc|ccc]
            1 & 0 & \hllc{2} & 3 & 0 & 0 \\
            0 & 1 & 0 & 0 & 2 & 0 \\
            0 & 0 & \hllr{4} & 4 & 3 & 1
        \end{pmatrix}
    \]
    \[
        \xrightarrow[\ 4F_3\ ]{\ F_1+3F_3\ }
        \begin{pmatrix}[ccc|ccc]
            1 & 0 & 0 & 1 & 1 & 2 \\
            0 & 1 & 0 & 0 & 2 & 0 \\
            0 & 0 & 1 & 1 & 2 & 4
        \end{pmatrix}
        \qquad \text{La matriz inversa es} \
        \begin{pmatrix}
            1 & 1 & 2 \\
            0 & 2 & 0 \\
            1 & 2 & 4
        \end{pmatrix}
        \hspace{160px}
    \]  

    \subsection{Propiedades de las matries inversas}
    Si $A$ es invertible y su inversa es $A^{-1}$:
    \begin{multicols}{2}
        \begin{itemize}
            \item $(A^{-1})^{-1}=A$
            \item $(kA)^{-1}=k^{-1}\cdot A^{-1}$
            \item $(A^t)^{-1}=(A^{-1})^t$
            \item Si $B$ es invertible: $(AB)^{-1}=B^{-1}\cdot A^{-1}$
        \end{itemize}
    \end{multicols}

    Las matrices elementales son invertibles:
    \begin{itemize}
        \item $(E_{ij})^{-1}=E_{ij}$
        \item $(E_i(k))^{-1}=E_i(k^{-1})$
        \item $(E_{i+(k)j})=E_{i+(-k)j}$
    \end{itemize}

    \section{El determinante}
    El determinante asocia a cada mariz cuadrada $A$ de orden $n$ un número determinado por los coeficientes de la matriz.
    \begin{itemize}
        \item Si orden $n=1$, definimos $det(A)=A_{11}$
        \item Para $n\geq2$ definidos los determinantes de cualquier matriz de orden $n-1$, se define para la matriz A:
        \[ a_{i1}A_{i1}+a_{i2}A_{i2}+...+a_{in}A_{in}\]
    \end{itemize}

    \subsection{Cálculo del determinante}
    \textbf{Regla de Sarrus:}

    \[+ \hspace{180px} -\]
    \begin{center}
        \begin{tikzpicture} [thick,scale=1.3, every node/.style={transform shape}]
            \draw (0,0) node[]{
            $\begin{pmatrix}
                a_{11} & a_{12} & a_{13} \\
                a_{21} & a_{22} & a_{23} \\
                a_{31} & a_{32} & a_{33}
            \end{pmatrix}$
            };
            \draw[Periwinkle, opacity=0.4, line width=7pt, line cap=round] (-1,0.5) -- (1,-0.6);
            \draw[Periwinkle, opacity=0.4, line width=7pt, line cap=round, rounded corners=8pt] (-1,0) -- (0,-0.7) -- (1.1,0.5) -- (-1,0);
            \draw[Periwinkle, opacity=0.4, line width=7pt, line cap=round, rounded corners=8pt] (-1,-0.5) -- (0,0.6) -- (1.1,0) -- (-1,-0.5);
            \begin{scope}[xshift=5cm]
                \draw (0,0) node[]{
                $ \begin{pmatrix}
                    a_{11} & a_{12} & a_{13} \\
                    a_{21} & a_{22} & a_{23} \\
                    a_{31} & a_{32} & a_{33}
                \end{pmatrix}$
                };
                \draw[Peach, opacity=0.4, line width=7pt, line cap=round] (-1,-0.5) -- (1.1,0.5);
                \draw[Peach, opacity=0.4, line width=7pt, line cap=round, rounded corners=8pt] (-1,0.5) -- (0,-0.7) -- (1.1,0) -- (-1,0.5);
                \draw[Peach, opacity=0.4, line width=7pt, line cap=round, rounded corners=8pt] (-1,0) -- (0,0.6) -- (1,-0.6)  -- (-1,0);
            \end{scope}
        \end{tikzpicture}
    \end{center}
    \[\hllc{a_{11}a_{22}a_{33}+a_{13}a_{21}a_{32}+a_{12}a_{23}a_{31}} \hllr{-a_{11}a_{23}a_{32}-a_{12}a_{21}a_{33}-a_{13}a_{22}a_{31}}\]

    El determinante verifica las sigueintes propiedades:
    \begin{multicols}{2}
        \begin{itemize}
            \item $det(A)=det(A^t)$
            \item $det(AB)=det(A)det(B)$
            \item $det(\lambda A)=\lambda^n det(A)$; $n= n^o$ filas/cols
            \item $det(A)det(A^{-1})=det(I_n)=1$
        \end{itemize}
    \end{multicols}
        \begin{itemize}
            \item Si la matriz $A$ tiene una fila o columna de ceros: $det(A)=0$
            \item Si una columna o fila de $A$ es múltiplo de otra: $det(A)=0$
            \item Si dos filas o columnas de $A$ son iguales: $det(A)=0$
            \item Si la matriz $A$ es triangular o diagonal: $det(A)= $ producto elementos de la diagonal
                \[det(A)=\prod_{i=1}^{n}a_{ii}\]
        \end{itemize}

        \subsection{Determinante y operaciones elementales}
        \begin{itemize}
            \item Si se intercambian dos filas o columnas de A, su determinante cambia de signo
            \item Si se multiplican los elementos de una fila o columna de A por un escalar k, su determinante queda multiplicado por k
            \item Si a una fila o columna de A se le suma otra fila o columna multiplicada por un escalar k no nulo, su determinante no varía
        \end{itemize}

        \[det(E_{ij})=-1; \qquad det(E_i(k))=k; \qquad det(E_{i(k)j})=1\]

\end{document}