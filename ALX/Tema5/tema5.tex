\documentclass[a4paper, 12pt, titlepage]{extarticle}
\usepackage[utf8]{inputenc}
\usepackage[spanish]{babel}
\usepackage{fancyhdr}

\setlength{\parindent}{0pt} % no indent paragraphs
\usepackage[margin=1in,bottom=1.2in,top=1.4in]{geometry}

\usepackage{amsmath, mathtools, amssymb, amsfonts, mathrsfs} %math
\usepackage{array}
\usepackage{multicol}
\usepackage{pifont} %symbols font

\usepackage{enumitem}
\usepackage{hyperref} %links
\usepackage{spalign} %systems
\spalignsysdelims{.}{\}} %system bracket on the right

% -- vertical line matrix
\makeatletter
\renewcommand*\env@matrix[1][*\c@MaxMatrixCols c]{%
  \hskip -\arraycolsep
  \let\@ifnextchar\new@ifnextchar
  \array{#1}}
\makeatother

% -- colors
\usepackage{soul}
\usepackage[dvipsnames]{xcolor}
\hypersetup{
    colorlinks,
    linkcolor={black},
    citecolor={blue!50!black},
    urlcolor={blue!80!black}
}

\definecolor{Lilac}{HTML}{c3cde6}

\newcommand{\hllc}[1]{\colorbox[HTML]{c3cde6}{$\displaystyle #1$}} % formato ancho cian
\newcommand{\hllr}[1]{\colorbox[HTML]{ffd7c6}{$\displaystyle #1$}} % formato ancho rosa
\newcommand{\hlly}[1]{\colorbox[HTML]{fff8dc}{$\displaystyle #1$}} % formato ancho amarillo
\newcommand{\hlfancy}[2]{\sethlcolor{#1}\hl{#2}} % no afecta al ancho del caracter

\usepackage{tikz}
\usetikzlibrary{decorations.pathreplacing}

\newcommand{\dsquare}{$\square \text{  }$}
\newcommand{\minisection}[2]{\dsquare\textbf{#1}\text{  #2}}
\newcommand{\enter}{\vspace{20px}}
\newcommand{\congr}{$\mathbb{Z}_m$}
\newcommand{\xmark}{\ding{55}}

%--- MAKETITLE ------------------------------------------------------------------------------

\def\myauthor{Clara Valle Gómez}
\author{\myauthor}

\title{
    \begin{Huge}
        {\textbf{ÁLGEBRA}}\\
    \end{Huge}
    \vspace{50px}
    \textbf{Tema V}\\
    Códigos lineales
    \date{\today}%\today
}

%--- CABECERA Y PIE DE PÁGINA -------------------------------------------------------------------------
\pagestyle{fancy}
\lhead{Tema 5}
\chead{}
\rhead{C. Valle} 
\lfoot{}
\cfoot{}
\rfoot{\thepage}    

%--- DOCUMENTO -------------------------------------------------------------------------------

\begin{document}
    \begin{figure}
        \centering
        %\vspace*{4cm}
        %\includegraphics[width=230px]{media/logo.png}
        %\caption{logo}
    \end{figure}
    \maketitle

    
%--- ÍNDICE -------------------------------------------------------------------------
    \newpage
    \tableofcontents

%--- DOC ----------------------------------------------------------------------------   
    \newpage
    \section{Códigos lineales}
    Un código lineal (n,k) es una aplicación inyectiva C: $(\mathbb{Z}_p)^k \rightarrow (\mathbb{Z}_p)^n$ cuya image, C, es el subespacio código. Sus elementos son vectores o palabras código. Los valores n y k son la longitud y dimensión del código respectivamente.

    \begin{itemize}
        \item El código de triple repetición C: $\mathbb{Z}_2\rightarrow (\mathbb{Z}_2)^3$, C($a)=(a,a,a)$, tiene longitud 3 y dimensión 1. El subespacio código es \{(0,0,0),(1,1,1)\}
        \item El código de triple paridad C: $(\mathbb{Z}_2)^3\rightarrow (\mathbb{Z}_2)^6$, C$(a_1,a_2,a_3)=(a_1,a_2,a_3,a_1+a_2,a_1+a_3,a_2+a_3)$, con long. 6 y dim. 3. El subespacio código es:
        \[
            \begin{array}{l}
                \{ (0,0,0,0,0,0),(1,0,0,1,1,0),(0,1,0,1,0,1),(0,0,1,0,1,1), \\
                \ \ (1,1,0,0,1,1),(1,0,1,1,0,1),(0,1,1,1,1,0),(1,1,1,0,0,0)\}
            \end{array}
        \]
    \end{itemize}

    \section{Matriz generadora}
    La matriz generadora de un código lineal, G, es la matriz asociada a la aplicación lineal C: $(C_p)^k \rightarrow (C_p)^n$ \textbf{cuyas columnas son las imágenes de la base canónica} $(\mathbb{Z}_p)^n$.\\

    \begin{multicols}{2}
        Matriz generadora C de triple repetición:
        \[
            G=
            \begin{pmatrix}
                1 \\
                1 \\
                1 \\
            \end{pmatrix}
        \]
        \columnbreak
        
        Matriz generadora C de triple paridad:
        \[
            G=
            \begin{pmatrix}
                1 & 0 & 0 \\
                0 & 1 & 0 \\
                0 & 0 & 1 \\
                1 & 1 & 0 \\
                1 & 0 & 1 \\
                0 & 1 & 1 \\
            \end{pmatrix}
        \]
        \columnbreak

    \end{multicols}

    $\hllr{\text{El nº de filas determina la longitud n, el nº de columnas la dimensión k, del código (n,k)}}$\\

    Un código lineal C es \textbf{sistemático} si las $k$ primeras filas de su matriz generadora G forman la matriz identidad de orden $k$, $I_k$.\\

    Un Código lineal C es \textbf{separable} si entre las filas de la matriz generadora G están presentes las filas de $I_k$, matriz identidad de orden $k$.\\

    $\Rightarrow$ \hyperref[sec:ej5.1]{Ejercicio 5.1}\\
    $\Rightarrow$ \hyperref[sec:ej5.2]{Ejercicio 5.2}\\
    $\Rightarrow$ \hyperref[sec:ej5.3]{Ejercicio 5.3}\\

    \section{Matriz control de paridad}

    Una matriz control de paridad H, de un código lineal (n,k) C es la matriz de coeficientes de un sistema de (n-k) ecuaciones homogéneas e independientes que caracteriza el subespacio código.

    Se llama síndrome de C a la aplicación lineal S:$(\mathbb{Z}_p)^n\rightarrow (\mathbb{Z}_p)^{(n-k)}$ cuya matriz asociada a las bases canónicas es H. Se llama $S(\vec{v})$, síndrome de v, a la imagen por S del vector v.

    \textit{Ejemplo:}
    \begin{itemize}
        \item Calculamos H y sídnromes del código de triple paridad:
    \end{itemize}

    Las columnas de la matriz G de triple paridad forman una base del subespacio código:

    \[
        rg
        \begin{pmatrix}
            1 & 0 & 0 & 1 & 1 & 0 \\
            0 & 1 & 0 & 1 & 0 & 1 \\
            0 & 0 & 1 & 0 & 1 & 1 \\
            x_1 & x_2 & x_3 & x_4 & x_5 & x_6\\
        \end{pmatrix}
        = rg(G) = 3
    \]
    Para tener rango 3 todos los elementos de la última fila deben ser nulos. Escalonamos:
    \[
        \begin{pmatrix}
            1 & 0 & 0 & 1 & 1 & 0 \\
            0 & 1 & 0 & 1 & 0 & 1 \\
            0 & 0 & 1 & 0 & 1 & 1 \\
            0 & 0 & 0 & x_1+x_2+x_4 & x_1+x_3+x_5 & x_2+x_3+x_6\\
        \end{pmatrix}
    \]
    \[
        \begin{cases}
            x_1+x_2+x_4=0 \\
            x_1+x_3+x_5=0 \\
            x_2+x_3+x_6=0 \\
        \end{cases}
        \qquad \text{obteniendo H= }
        \begin{pmatrix}
            1 & 1 & 0 & 1 & 0 & 0 \\
            1 & 0 & 1 & 0 & 1 & 0 \\
            0 & 1 & 1 & 0 & 0 & 1 \\
        \end{pmatrix}
    \]
    \[S(1,1,0,0,1,1)=(0,0,0) \quad \rightarrow \quad (1,1,0,0,1,1)\in Im(C) \text{ , es un vector código}\]\\

    $\Rightarrow$ \hyperref[sec:ej5.4]{Ejercicio 5.4}

    \section{Peso y distancia de Hamming}
    El \textbf{peso} de un vector $\vec{v}$ denotado por $w(\vec{v})$ es el \textbf{número de entradas no nulas} de $\vec{v}$.

    La distancia de Hamming $d(\vec{v},\vec{u})$ entre dos vectores es el peso del vector $\vec{v}$ - $\vec{u}$, por lo tanto $d(\vec{v},\vec{u})=w(\vec{v}-\vec{u})$\\

    El \textbf{peso mínimo} de un código lineal C, $w(C)$, es el \textbf{menor de los pesos} de los vectores código no nulos.\\

    \subsection{Errores}

    Sea $\vec{c}\in Im(C)$ un vector recibido como el vector $\vec{r}$, se define el vector error producido en la transmisión de $\vec{c}$ como el vector $\vec{e}=\vec{r}-\vec{c}$, equivalentemente $\vec{r}=\vec{c}+\vec{e}$. \\

    \textit{Ejemplos:}
    \begin{itemize}
        \item Supongamos que en un canal binario se transmite un vector $\vec{c}=0001$ y se recibe el vector $\vec{r}=0011$
    \end{itemize}
    El error producido es $\vec{e}=0010$ y tiene peso $w(0010)=1$, es decir un error simple\\

    $\Rightarrow$ \hyperref[sec:ej5.7]{Ejercicio 5.7}\\
    $\Rightarrow$ \hyperref[sec:ej5.8]{Ejercicio 5.8}

    \section{Detección y corrección de errores}
    Sabemos que un vector $\vec{v}$ es código si, y solo si, su síndrome es el vector 0, $S(\vec{v})=\vec{0}$, por eso el vector recibido $\vec{r}$ tiene el mismo síndrome que el error que se haya producido.
    \[
        S(\vec{r})=S(\vec{c})+S(\vec{e})=S(\vec{e})
    \]

    Un código lineal C \textbf{detecta cualquier error de peso menor o igual que $\mathbf{\lambda}$ su y solo si su peso mínimo es mayor que $\mathbf{\lambda}$}\\

    \subsection{Tabla de síndromes y decodificación por peso mínimo}

    Dado un código lineal C con matriz de paridad H, a cada vector $\vec{s}$ se le asocia un representante del síndrome, con síndrome $\vec{s}_i$.La tabla de síndromes se construye con $\hllr{P^{n-k}}$ filas\\

    \textit{Ejemplo:}
    \begin{itemize}
        \item Sea C el código binario de triple paridad con H:
    \end{itemize}
    \[
        H=
        \begin{pmatrix}
            \hllc{1} & 1 & 0 & 1 & 0 & 0 \\
            \hllc{1} & 0 & 1 & 0 & 1 & 0 \\
            \hllc{0} & 1 & 1 & 0 & 0 & 1 \\
        \end{pmatrix}
    \]
    Como C es un (6,3) código lineal binario, la tabla tiene $p^{n-k}=2^3=8$ filas:

    \begin{center}
        \begin{tabular}{|c|c|}
            \hline
            Representante & Síndrome \\ \hline
            000000        & 000      \\ \hline
            100000        & \hlfancy{Lilac}{110}      \\ \hline
            010000        & 101      \\ \hline
            001000        & 011      \\ \hline
            000100        & 100      \\ \hline
            000010        & 010      \\ \hline
            000001        & 001      \\ \hline
            100001        & 111      \\ \hline
            \end{tabular}
    \end{center}

    Si recibimos un vector $\vec{r}=101000$ con síndrome $S(\vec{r})=101$. El representante de 101 es $\vec{v}=010000$. Por lo que el código transmitido es: $\vec{c}=\vec{r}-\vec{v}=101000-010000=111000$\\

    La \textbf{decodificación por peso mínimo} consiste en elegir como representante de cada síndrome el vector de menos peso posible. Si hay más de una elección posible se elige uno de ellos de forma arbitraria.\\

    \textit{Ejemplo:}
    \begin{itemize}
        \item Calcula la longitud, dimensión y una tabla de síndromes para un código c con H:
    \end{itemize}
    \[
        H=
        \begin{pmatrix}
            0 & 0 & 0 & 1 & 0 & 0 & 0 \\
            0 & 1 & 0 & 0 & 0 & 1 & 0 \\
            1 & 0 & 1 & 0 & 1 & 0 & 1 \\
        \end{pmatrix}
    \]\\

    Tiene longitud 7 (nº cols) y dimensión 4 (nº cols $-$ nº filas), por lo que la tabla tiene $2^{7-4}=8$ filas.

    \begin{center}
        \begin{tabular}{|c|c|}
            \hline
            Representante & Síndrome \\ \hline
            0000000       & 000      \\ \hline
            1000000       & 001      \\ \hline
            0100000       & 010      \\ \hline
            0001000       & 100      \\ \hline
            1100000       & 011      \\ \hline
            1001000       & 101      \\ \hline
            0101000       & 110      \\ \hline
            1101000       & 111      \\ \hline
            \end{tabular}
    \end{center}


    Utilizando la decodificación por peso mínimo, un código lineal C corrige errores de peso menor o igual que $\lambda$ solo si su peso mínimo es mayor que $2\lambda$

    \subsection{Tabla incompleta de síndromes}
    Si sabemos que un código C tiene un peso mínimo $> 2\lambda$, por lo que corregiremos errores de peso menor o igual, se puede considerar una tabla incompleta en la cual solo habría representantes de estos pesos.\\

    Si el síndrome recibido $S(\vec{r})$ no aparece en la tabla, es que tiene más de $\lambda$ errores y no es posible corregirlos.\\

    $\Rightarrow$ \hyperref[sec:ej5.9]{Ejercicio 5.9}\\
    $\Rightarrow$ \hyperref[sec:ej5.12]{Ejercicio 5.12}

    \section{Códigos Hamming}
    Para un número natural m \textbf{consideramos H} la matriz binaria de orden m $\times (2^m-1)$ \textbf{cuyas columnas son todos los vectores} no nulos de $(\mathbb{Z}_2)^m$ \textbf{colocados de forma que la j-ésima columna es la representación binaria del número natural j}. Un código lineal binario que tenga H como matriz control de paridad se denomina código Hamming binario, $\mathbf{H(m,2)}$.\\

    Los códigos hamming binarios tienen peso mínimo 3 y corrige errores de peso 1.\\

    La decodificación de códigos Hamming consiste en corregir errores simples ya que un error con úna única entrada no nula en la posición j, su síndrome es la j-ésima columna de H\\

    Para calcular la matriz generadora de un código Hamming se resuelve un sistema de ecuaciones lineales con $2^m-1$ incógnitas cuya matriz de coeficientes es H y se despejan las incógnitas en las posiciones de potencias de 2 (dígitos de control).\\

    \textit{Ejemplo:}
    \begin{itemize}
        \item Calcula la matiz generadora del cógio de Hamming (3,2)
    \end{itemize}
    Para m=3 la matriz control de paridad H es $3\times (2^3-1)$:
    \[
        H=
        \begin{pmatrix}
            0 & 0 & 0 & 1 & 1 & 1 & 1 \\
            0 & 1 & 1 & 0 & 0 & 1 & 1 \\
            1 & 0 & 1 & 0 & 1 & 0 & 1 \\
        \end{pmatrix}
    \]
    Resolvemos el sistema:
    \[
        HX=
        \begin{pmatrix}
            0 & 0 & 0 & \mathbf{1} & 1 & 1 & 1 \\
            0 & \mathbf{1} & 1 & 0 & 0 & 1 & 1 \\
            \mathbf{1} & 0 & 1 & 0 & 1 & 0 & 1 \\
        \end{pmatrix}
        \cdot
        \begin{pmatrix}
            x_1 \\
            x_2 \\
            x_3 \\
            x_4 \\
            x_5 \\
            x_6 \\
            x_7 \\
        \end{pmatrix}
        =
        \begin{pmatrix}
            0 \\
            0 \\
            0 \\
        \end{pmatrix}
    \]
    \[
        \begin{cases}
            \mathbf{x_4}+x_5+x_6+x_7=0\\
            \mathbf{x_2}+x_3+x_6+x_7=0\\
            \mathbf{x_1}+x_3+x_5+x_7=0\\
        \end{cases}
        \xrightarrow[]{\text{Depejando las variables ligadas}} \
        \begin{cases}
            \mathbf{x_4}= x_5+x_6+x_7\\
            \mathbf{x_2}= x_3+x_6+x_7\\
            \mathbf{x_1}= x_3+x_5+x_7\\
        \end{cases}
    \]
    Los vectores códigos son de la forma:
    \[(x_1,x_2,x_3,x_4,x_5,x_6,x_7)=(\mathbf{x_3+x_5+x_7},\mathbf{x_3+x_6+x_7},x_3,\mathbf{x_5+x_6+x_7},x_5,x_6,x_7) = \]
    \[=x_3(1,1,1,0,0,0,0)+x_5(1,0,0,1,1,0,0)+x_6(0,1,0,1,0,1,0)+x_7(1,1,0,1,0,0,1)\]
    \[Base: {(1,1,1,0,0,0,0),\ (1,0,0,1,1,0,0),\ (0,1,0,1,0,1,0),\ (1,1,0,1,0,0,1)}\]
    Colocando los vectores en columnas se obtiene la matriz generadora del código.\\

    \subsection{Código Hamming ampliado}
    Un código de Hamming ampliado $HA(m,2)$ añade a cada vector código de $H(m,2)$ un dígito de control de paridad el final (1 o 0) para que el peso de cada vector en el nuevo código sea par.\\

    La longitud de $HA(m,2)$ es $n=2^m$ y su dimensión es $k=2^m-m-1$ y el peso mínimo de $HA(m,2)$ pasa a ser 4, por lo que ahora \textbf{detecta} (no corrige) errores dobles.\\

    La \textbf{matriz de control de paridad} de un código $HA(m,2)$, $H^*$, \textbf{se obtiene a partir de H añadiendo una columna de ceros a la derecha y una última fila de unos}.\\

    La matriz generadora de un código HA(m,2), $G^*$, se obtiene añdiendo una última fila con la suma binaria (1 o 0) de los dígitos de cada columna de la matriz G\\


    \textit{Ejemplo:}\\
    A partir de código $H(3,2)$ su código ampliado $HA(3,2)$ tendría:
    \[
        H^*=
        \begin{pmatrix}
            0 & 0 & 0 & 1 & 1 & 1 & 1 & \mathbf{0} \\
            0 & 1 & 1 & 0 & 0 & 1 & 1 & \mathbf{0}\\
            1 & 0 & 1 & 0 & 1 & 0 & 1 & \mathbf{0}\\
            \mathbf{1} & \mathbf{1} & \mathbf{1} & \mathbf{1} & \mathbf{1} & \mathbf{1} & \mathbf{1} & \mathbf{1}\\
        \end{pmatrix}
    \]
    Dada su matriz $G$ obtenemos $G^*$:
    \[
        G=
        \begin{pmatrix}
            1 & 1 & 0 & 1 \\
            1 & 0 & 1 & 1 \\
            1 & 0 & 0 & 0 \\
            0 & 1 & 1 & 1 \\
            0 & 1 & 0 & 0 \\
            0 & 0 & 1 & 0 \\
            0 & 0 & 0 & 1 \\
        \end{pmatrix}
        \qquad \longrightarrow \qquad
        G^*=
        \begin{pmatrix}
            1 & 1 & 0 & 1 \\
            1 & 0 & 1 & 1 \\
            1 & 0 & 0 & 0 \\
            0 & 1 & 1 & 1 \\
            0 & 1 & 0 & 0 \\
            0 & 0 & 1 & 0 \\
            0 & 0 & 0 & 1 \\
            \mathbf{1} & \mathbf{1} & \mathbf{1} & \mathbf{0}
        \end{pmatrix}
    \]

    La decodificación de códigos Hamming ampliados, al ser su peso mínimo 4 permite detectar errores dobles. Todo vector con un error simple, su síndrome termina en 1, por otra parte, un error doble (dos entradas no nulas), su síndrome termina en 0. Obtenemos así un algotiritmo de decodificación:

    \begin{enumerate} [label=\alph*)]
        \item Si $s_{m+1}=0$ y
        \begin{itemize}
            \item $\vec{s}=\vec{0}$ es el vector código enviado
            \item $\vec{s}\not=\vec{0}$ error al menos doble no posible corregir
        \end{itemize}
        \item Si $s_{m+1}=1$ y
        \begin{itemize}
            \item $\vec{s}=\vec{0}$ error simple en la n-ésima (última) posición
            \item $\vec{s}\not=\vec{0}$ error simple en la j-ésima posición
        \end{itemize}
    \end{enumerate}

    \textit{Ejemplo:}
    \begin{itemize}
        \item Consideramos el código Hamming ampliado $HA(3,2)$ con matriz generadora $G^*$:
    \end{itemize}
    \[
        G^*=
        \begin{pmatrix}
            1 & 1 & 0 & 1 \\
            1 & 0 & 1 & 1 \\
            1 & 0 & 0 & 0 \\
            0 & 1 & 1 & 1 \\
            0 & 1 & 0 & 0 \\
            0 & 0 & 1 & 0 \\
            0 & 0 & 0 & 1 \\
            \mathbf{1} & \mathbf{1} & \mathbf{1} & \mathbf{0} \\
        \end{pmatrix}
    \]
    El vector $\vec{u}=1011$ codificado en H(3,2) como $\vec{c}=0110011$ se codifica ahora como $\vec{c}\ ^*=01100110$.\\

    Si  $\vec{c}\ ^*$ es recibido como $\vec{r}=01100\textcolor{Maroon}{0}10$ su síndrome $S(\vec{r}=110|1)$ nos indica que se ha producido un error simple en la sexta posición (en binario 110).\\

    Si  $\vec{c}\ ^*$ es recibido como $\vec{r}=0110011\textcolor{Maroon}{1}$ su síndrome $S(\vec{r}=000|1)$ nos indica que se ha producido un error simple en la última posición\\

    Si  $\vec{c}\ ^*$ es recibido como $\vec{r}=0\textcolor{Maroon}{0}100\textcolor{Maroon}{0}10$ su síndrome $S(\vec{r}=100|0)$ nos indica que se ha producido un error al menos doble que no se puede corregir\\

    $\Rightarrow$ \hyperref[sec:ej5.14]{Ejercicio 5.14}\\
    $\Rightarrow$ \hyperref[sec:ej5.15]{Ejercicio 5.15}\\
    $\Rightarrow$ \hyperref[sec:ej5.16]{Ejercicio 5.16}\\












    \newpage
    \section{Ejercicios}
    \subsection{Ejercicio 5.1}
    \label{sec:ej5.1}

    Calgula long, dim, subespacio código y matriz G del código binario de triple repetición C: $(\mathbb{Z}_2)^3 \rightarrow (\mathbb{Z}_2)^9$ definido por C: $(a_1,a_2,a_3)=(a_1,a_2,a_3,a_1,a_2,a_3,a_1,a_2,a_3)$\\

    Dimensión 3, longitud 9.
    \[
        G=
        \begin{pmatrix}
            1 & 0 & 0 \\
            0 & 1 & 0 \\
            0 & 0 & 1 \\
            1 & 0 & 0 \\
            0 & 1 & 0 \\
            0 & 0 & 1 \\
            1 & 0 & 0 \\
            0 & 1 & 0 \\
            0 & 0 & 1 \\
        \end{pmatrix}
        \quad
       \text{subespacio C: }
       \begin{array}{l}
           C:(0,0,0) = (0,0,0,0,0,0,0,0,0) \\
           C:(1,0,0) = (1,0,0,1,0,0,1,0,0) \\
           C:(0,1,0) = (0,1,0,0,1,0,0,1,0) \\
           C:(0,0,1) = (0,0,1,0,0,1,0,0,1) \\
           C:(1,1,0) = (1,1,0,1,1,0,1,1,0) \\
           C:(0,1,1) = (0,1,1,0,1,1,0,1,1) \\
           C:(1,0,1) = (1,0,1,1,0,1,1,0,1) \\
           C:(1,1,1) = (1,1,1,1,1,1,1,1,1) \\
       \end{array}
    \]

    \subsection{Ejercicio 5.2}
    \label{sec:ej5.2}

    Calgula long, dim, subespacio código, matriz G y justificar si es sistemático o separable, del código binario C: $(\mathbb{Z}_2)^4 \rightarrow (\mathbb{Z}_2)^9$ definido por C: $(a_1,a_2,a_3,a_4)=(a_1,a_2,a_3,a_4,a_1+a_2,a_3+a_4,a_1+a_3,a_2+a_4,a_1+a_2+a_3+a_4)$\\

    Dimensión 4, longitud 9.
    \[
        G=
        \begin{pmatrix}
            1 & 0 & 0 & 0 \\
            0 & 1 & 0 & 0 \\
            0 & 0 & 1 & 0 \\
            0 & 0 & 0 & 1 \\
            1 & 1 & 0 & 0 \\
            0 & 0 & 1 & 1 \\
            1 & 0 & 1 & 0 \\
            0 & 1 & 0 & 1 \\
            1 & 1 & 1 & 1 \\
        \end{pmatrix}
        \quad
       \text{subespacio C: }
       \begin{array}{l}
           C:(0,0,0,0) = (0,0,0,0,0,0,0,0,0) \\
           C:(1,0,0,0) = (1,0,0,0,1,0,1,0,1) \\
           C:(0,1,0,0) = (0,1,0,0,1,0,0,1,1) \\
           C:(0,0,1,0) = (0,0,1,0,0,1,1,0,1) \\
           C:(0,0,0,1) = (0,0,0,1,0,1,0,1,1) \\
           C:(1,1,0,0) = (1,1,0,0,0,1,0,1,0) \\
           C:(0,0,1,1) = (0,0,1,1,0,0,1,1,0) \\
           \ \ \quad \qquad \vdots \\
           C:(1,1,1,1) = (1,1,1,1,0,0,0,0,0) \\
       \end{array}
    \]
    Es sistemático ya que las primeras filas de la matriz forman la matriz identidad $I_4$.

    \subsection{Ejercicio 5.3}
    \label{sec:ej5.3}

    Justifica si la siguiente matriz es una matriz generadora, indicando longitud y dimensión.
    \[
        G^t=
        \begin{pmatrix}
            0 & 1 & 0 & 0 & 1 & 1 & 1 & 0 & 1 \\
            1 & 0 & 0 & 1 & 0 & 1 & 1 & 0 & 1 \\
            1 & 0 & 1 & 1 & 0 & 0 & 1 & 1 & 0 \\
            1 & 0 & 1 & 1 & 0 & 1 & 1 & 0 & 1 \\
        \end{pmatrix}
    \]
    Generaría un código $(\mathbb{Z}_2)^4\rightarrow(\mathbb{Z}_2)^9$ de dimensión 4 y longitud 9.

    \subsection{Ejercicio 5.4}
    \label{sec:ej5.4}
    Calcula una matriz control de paridad para los códigos de los ejercicios 5.1, 5.2 y 5.3.

    \begin{itemize}
        \item Ejercicio 5.1
    \end{itemize}
    \[
        \ rg=
        \begin{pmatrix}
            1 & 0 & 0 & 1 & 0 & 0 & 1 & 0 & 0 \\
            0 & 1 & 0 & 0 & 1 & 0 & 0 & 1 & 0 \\
            0 & 0 & 1 & 0 & 0 & 1 & 0 & 0 & 1 \\
            x_1 & x_2 & x_3 & x_4 & x_5 & x_6 & x_7 & x_8 & x_9 \\ 
        \end{pmatrix}
        =3 
        \xrightarrow[]{F_4+x_1\cdot F1+x_2\cdot F2+x_3\cdot F3}
    \]
    \[
        \text{Esc. Re:}
        \begin{pmatrix}
            1 & 0 & 0 & 1 & 0 & 0 & 1 & 0 & 0 \\
            0 & 1 & 0 & 0 & 1 & 0 & 0 & 1 & 0 \\
            0 & 0 & 1 & 0 & 0 & 1 & 0 & 0 & 1 \\
            0 & 0 & 0 & x_1+x_4 & x_2+x_5 & x_3+x_6 & x_1+x_7 & x_2+x_8 & x_3+x_9 \\ 
        \end{pmatrix}
    \]
    \[
        \begin{cases}
            x_1+x_4=0 \\
            x_2+x_5=0 \\
            x_3+x_6=0 \\
            x_1+x_7=0 \\
            x_2+x_8=0 \\
            x_3+x_9=0 \\
        \end{cases} 
        \qquad
        H=
        \begin{pmatrix}
            1 & 0 & 0 & 1 & 0 & 0 & 0 & 0 & 0 \\
            0 & 1 & 0 & 0 & 1 & 0 & 0 & 0 & 0 \\
            0 & 0 & 1 & 0 & 0 & 1 & 0 & 0 & 0 \\
            1 & 0 & 0 & 0 & 0 & 0 & 1 & 0 & 0 \\
            0 & 1 & 0 & 0 & 0 & 0 & 0 & 1 & 0 \\
            0 & 0 & 1 & 0 & 0 & 0 & 0 & 0 & 1 \\
        \end{pmatrix}
    \]

    \begin{itemize}
        \item Ejercicio 5.2
    \end{itemize}
    \[
        \ rg=
        \begin{pmatrix}
            1 & 0 & 0 & 0 & 1 & 0 & 1 & 0 & 1 \\
            0 & 1 & 0 & 0 & 1 & 0 & 0 & 1 & 1 \\
            0 & 0 & 1 & 0 & 0 & 1 & 1 & 0 & 1 \\
            0 & 0 & 0 & 1 & 0 & 1 & 0 & 1 & 1 \\
            x_1 & x_2 & x_3 & x_4 & x_5 & x_6 & x_7 & x_8 & x_9\\
        \end{pmatrix}
        =4
        \xrightarrow[]{F_5+x_1\cdot F1+x_2\cdot F2+x_3\cdot F3 +x_4\cdot F4}
    \]
    \[
        \begin{pmatrix}
            1 & 0 & 0 & 0 & 1 & 0 & 1 & 0 & 1 \\
            0 & 1 & 0 & 0 & 1 & 0 & 0 & 1 & 1 \\
            0 & 0 & 1 & 0 & 0 & 1 & 1 & 0 & 1 \\
            0 & 0 & 0 & 1 & 0 & 1 & 0 & 1 & 1 \\
            0 & 0 & 0 & 0 & x_1+x_2+x_5 & x_3+x_4+x_6 & x_1+x_3+x_7 & x_2+x_4+x_8 & x_1+x_2+x_3+x_4+x_9 \\
        \end{pmatrix}
    \]
    \[
        \begin{cases}
            x_1+x_2+x_5=0 \\
            x_3+x_4+x_6=0 \\
            x_1+x_3+x_7=0 \\
            x_2+x_4+x_8=0 \\
            x_1+x_2+x_3+x_4+x_9=0 \\
        \end{cases} 
        \qquad
        H=
        \begin{pmatrix}
            1 & 1 & 0 & 0 & 1 & 0 & 0 & 0 & 0 \\
            0 & 0 & 1 & 1 & 0 & 1 & 0 & 0 & 0 \\
            1 & 0 & 1 & 0 & 0 & 0 & 1 & 0 & 0 \\
            0 & 1 & 0 & 1 & 0 & 0 & 0 & 1 & 0 \\
            1 & 1 & 1 & 1 & 0 & 0 & 0 & 0 & 1 \\
        \end{pmatrix}
    \]

    \newpage
    \subsection{Ejercicio 5.7}
    \label{sec:ej5.7}
    Calcula el peso Hamming de cada uno de los siguientes vectores:
    \begin{multicols}{3}
        \begin{itemize}
            \item $w(10101)=3$
        \end{itemize}
        \columnbreak
        \begin{itemize}
            \item $w(11000)=2$
        \end{itemize}
        \columnbreak
        \begin{itemize}
            \item $w(01110)=3$
        \end{itemize}
    \end{multicols}

    \subsection{Ejercicio 5.8}
    \label{sec:ej5.8}
    Calcula el peso mínimo del código lineal ternario C con
    \[
        G =
        \begin{pmatrix}
            0 & 1 \\
            1 & 0 \\
            1 & 1 \\
            2 & 1 \\
        \end{pmatrix}
        \qquad
        \begin{array}[]{l}
            \mathbf{C(0,1) = (1,0,1,1)}\\
            \mathbf{C(1,0) = (0,1,1,2)}\\
            C(1,1) = (1,1,2,0)\\
            C(2,2) = (2,2,1,0)\\
        \end{array}
        \qquad
        \begin{array}[]{l}
            C(0,2) = (2,0,2,2)\\
            C(2,0) = (0,2,1,2)\\
            C(1,2) = (2,1,0,1)\\
            C(2,1) = (1,2,2,0)\\
        \end{array}
    \]
    \begin{center}
        PesoMin=pesoMax=3
    \end{center}

    \subsection{Ejercicio 5.9}
    \label{sec:ej5.9}
    Calcula una tabla de síndromes para el código ternario con matriz H y decodifica los vectores: 2121, 2222, 1002, 0021.
    \[
        H=
        \begin{pmatrix}
            1 & 2 & 0 & 1 \\
            0 & 2 & 1 & 0 \\
        \end{pmatrix}
    \]

    Tiene longitud 4 y dimensión 2, por lo que la tabla tiene $3^{4-2}=9$ filas

    \begin{center}
        \begin{tabular}{|c|c|}
            \hline
            Representante & Síndrome \\ \hline
            0000       & 00      \\ \hline
            1000       & 10      \\ \hline
            2000       & 20      \\ \hline
            0100       & 22      \\ \hline
            0200       & 11      \\ \hline
            0010       & 01      \\ \hline
            0020       & 02      \\ \hline
            0001       & 12      \\ \hline
            0002       & 21      \\ \hline
            \end{tabular}
    \end{center}
    \[
        \begin{array}[]{l}
            S(2121)=20 \rightarrow \vec{e}=2000 \rightarrow c:0121\\
            S(2222)=21 \rightarrow \vec{e}=0002 \rightarrow c:2220\\
            S(1002)=01 \rightarrow \vec{e}=0010 \rightarrow c:1022\\
            S(0021)=11 \rightarrow \vec{e}=0200 \rightarrow c:0121\\
        \end{array}
    \]

    \newpage
    \subsection{Ejercicio 5.12}
    \label{sec:ej5.12}
    Utilizando la decodificación por peso mínimo:
    \begin{enumerate} [label=\alph*)]
        \item Que capacidad correctora tiene un código lineal cuyo peso mínimo es 5?\\
        Corregiría errores dobles ya que $w(c)=5 > 2\cdot 2 = 4$
        \item Que peso mínimo debe tener un código para corregir 6 errores?\\
        debe tener peso mínimo 13 ya que $w(c)=13 > 2\cdot 6= 12$
        \item Que peso mínimo debe tener un código lineal para detectar 4 erroes?\\
        ya que $w(c)\geq$ nº errores, entonces $w(c)=4\geq 5$
    \end{enumerate}

    \subsection{Ejercicio 5.14}
    \label{sec:ej5.14}
    Codifica usando los códigos Hamming $H(3,2)$ y $HA(3,2)$ los siguientes vectores $\vec{m}$:

    \[
        H=
        \begin{pmatrix}
            0 & 0 & 0 & 1 & 1 & 1 & 1 \\
            0 & 1 & 1 & 0 & 0 & 1 & 1 \\
            1 & 0 & 1 & 0 & 1 & 0 & 1 \\
        \end{pmatrix}
        \qquad
        H^*=
        \begin{pmatrix}
            0 & 0 & 0 & 1 & 1 & 1 & 1 & \mathbf{0}\\
            0 & 1 & 1 & 0 & 0 & 1 & 1 & \mathbf{0}\\
            1 & 0 & 1 & 0 & 1 & 0 & 1 & \mathbf{0}\\
            \mathbf{1} & \mathbf{1} & \mathbf{1} & \mathbf{1} & \mathbf{1} & \mathbf{1} & \mathbf{1} & \mathbf{1}\\
        \end{pmatrix}
    \]
    \[
        \begin{cases}
            x_4+x_5+x_6+x_7=0\\
            x_2+x_3+x_6+x_7=0\\
            x_1+x_3+x_5+x_7=0\\
        \end{cases}
        \quad \rightarrow \quad
        \begin{cases}
            x_4=x_5+x_6+x_7\\
            x_2=x_3+x_6+x_7\\
            x_1=x_3+x_5+x_7\\
        \end{cases} 
    \]
    \[
        \text{base: }\{(1,1,1,0,0,0,0),(1,0,0,1,1,0,0),(0,1,0,1,0,1,0),(1,1,0,1,0,0,1)\}
    \]
    \[
        G=
        \begin{pmatrix}
            1 & 1 & 0 & 1 \\
            1 & 0 & 1 & 1 \\
            1 & 0 & 0 & 0 \\
            0 & 1 & 1 & 1 \\
            0 & 1 & 0 & 0 \\
            0 & 0 & 1 & 0 \\
            0 & 0 & 0 & 1 \\
        \end{pmatrix}
        \qquad \longrightarrow \qquad
        G^*=
        \begin{pmatrix}
            1 & 1 & 0 & 1 \\
            1 & 0 & 1 & 1 \\
            1 & 0 & 0 & 0 \\
            0 & 1 & 1 & 1 \\
            0 & 1 & 0 & 0 \\
            0 & 0 & 1 & 0 \\
            0 & 0 & 0 & 1 \\
            \mathbf{1} & \mathbf{1} & \mathbf{1} & \mathbf{0}
        \end{pmatrix}
    \]\\
    \begin{multicols}{2}
        \[
            \begin{array}[]{l}
                C:(1000)=1110000 \\
                C:(0100)=1001100 \\
                C:(0010)=0101010 \\
                C:(0001)=1101001 \\
            \end{array}
        \]
        \columnbreak
        \begin{center}
            \setlength{\tabcolsep}{10pt}
            \renewcommand{\arraystretch}{1.2}
        
            \begin{tabular}{|c|c|c|}
                \hline
                $\vec{m}$  & H(3,2) & HA(3,2) \\ \hline
                1011 & 0110011 & 01100110      \\ \hline
                0111 & 0001111 & 00011110      \\ \hline
                0011 & 1000011 & 10000111      \\ \hline
                1110 & 0010110 & 00101101      \\ \hline
            \end{tabular}
        \end{center}
    \end{multicols}

    \subsection{Ejercicio 5.15}
    \label{sec:ej5.15}
    Decodifica los siguientes vectores recibidos y calcula el mensaje sabiendo que se ha usado un código Hamming $H(3,2)$

    \[
        H=
        \begin{pmatrix}
            0 & 0 & 0 & 1 & 1 & 1 & 1 \\
            0 & 1 & 1 & 0 & 0 & 1 & 1 \\
            1 & 0 & 1 & 0 & 1 & 0 & 1 \\
        \end{pmatrix}
    \]\\

    El síndrome se calcula multiplicando las posiciones en binario sus valores no nulos. 
    
    Las potencias de dos son dígutos de control, mientras que el resto son los dígitos de información del  mensaje enviado $\vec{m}$.
    \begin{itemize}
        \item $\vec{r}=1101011 \quad \rightarrow \quad S(1101011)=110 \quad \rightarrow \quad \vec{e}=0000010 \quad \rightarrow \quad \vec{c}=\textcolor{Maroon}{11}0\textcolor{Maroon}{1}001$\\
        \[\vec{m}=0001\]
        \item $\vec{r}=1111111 \quad \rightarrow \quad S(1111111)=000 \quad \rightarrow \qquad \text{correcto} \ \ \quad \rightarrow \quad \vec{c}=\textcolor{Maroon}{11}1\textcolor{Maroon}{1}111$\\
        \[\vec{m}=1111\]
        \item $\vec{r}=0011010 \quad \rightarrow \quad S(0011010)=001 \quad \rightarrow \quad \vec{e}=1000000 \quad \rightarrow \quad \vec{c}=\textcolor{Maroon}{10}1\textcolor{Maroon}{1}010$\\
        \[\vec{m}=1010\]
        \item $\vec{r}=0100011 \quad \rightarrow \quad S(0100011)=011 \quad \rightarrow \quad \vec{e}=0010000 \quad \rightarrow \quad \vec{c}=\textcolor{Maroon}{01}1\textcolor{Maroon}{0}011$\\
        \[\vec{m}=1011\]
    \end{itemize}
    \subsection{Ejercicio 5.16}
    \label{sec:ej5.16}
    Decodifica los siguientes vectores recibidos y calcula el mensaje teniendo en cuenta que se ha usado un código Hamming HA(3,2)
    \[
        H^*=
        \begin{pmatrix}
            0 & 0 & 0 & 1 & 1 & 1 & 1 & \mathbf{0} \\
            0 & 1 & 1 & 0 & 0 & 1 & 1 & \mathbf{0}\\
            1 & 0 & 1 & 0 & 1 & 0 & 1 & \mathbf{0}\\
            \mathbf{1} & \mathbf{1} & \mathbf{1} & \mathbf{1} & \mathbf{1} & \mathbf{1} & \mathbf{1} & \mathbf{1}\\
        \end{pmatrix}
    \]





\end{document}