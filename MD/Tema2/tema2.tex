\documentclass[a4paper, 12pt, titlepage]{extarticle}
\newcommand\tab[1][1cm]{\hspace*{#1}}
\usepackage[utf8]{inputenc}
\usepackage[spanish]{babel}
\usepackage{fancyhdr}

\setlength{\parindent}{0pt}
\usepackage[margin=1in,bottom=1.2in,top=1.4in]{geometry}

\usepackage{amsmath, mathtools, amssymb, amsfonts, mathrsfs, array} %math
\usepackage{multicol}
\usepackage{pifont}
\usepackage{enumitem}

\usepackage{hyperref} %links
\usepackage{soul}
\usepackage[dvipsnames]{xcolor}
\hypersetup{
    colorlinks,
    linkcolor={black},
    citecolor={blue!50!black},
    urlcolor={blue!80!black}
}
\definecolor{Lilac}{HTML}{c3cde6}

\newcommand{\hllc}[1]{\colorbox[HTML]{c3cde6}{$\displaystyle #1$}}
\newcommand{\hllr}[1]{\colorbox[HTML]{ffd7c6}{$\displaystyle #1$}} % formato ancho rosa
\newcommand{\hlfancy}[2]{\sethlcolor{#1}\hl{#2}}

\usepackage{tikz}
\usetikzlibrary{tikzmark}
\usepackage[siunitx]{circuitikz}

\tikzset{filled/.style={fill=Lilac, draw=Periwinkle, thick}, outline/.style={draw=Periwinkle, thick}}

\newcommand{\xmark}{\ding{55}}
\newcommand{\enter}{\vspace{10px}}

%--- MAKETITLE ------------------------------------------------------------------------------

\def\myauthor{Clara Valle Gómez}
\author{\myauthor}

\title{
    \begin{Huge}
        {\textbf{MATEMÁTICA DISCRETA}}\\
    \end{Huge}
    \vspace{50px}
    \textbf{Tema II}\\
    Conjuntos y aplicaciones
    \date{\today}
}

%--- CABECERA Y PIE DE PÁGINA -------------------------------------------------------------------------
\setlength{\headheight}{16.00pt} %warning
\pagestyle{fancy}
\lhead{Tema 2}
\chead{}
\rhead{C.Valle} 
\lfoot{}
\cfoot{}
\rfoot{\thepage}    

\begin{document}
\maketitle

    
%--- ÍNDICE -----------------------------------------------------------
    \newpage
    \tableofcontents

%--- DOC ---------------------------------------------------------- 
    \newpage
    \section{Conjuntos}
    Un conjunto ($A$) es una colección de objetos distinguibles entre si, son \textbf{elementos} ($a$), cuando un elemento pertenece a un conjunto se escribe $a\in A$, y si no $a\not\in A$. Una colección sin elementos es un \textbf{conjunto vacío} $\emptyset$ o \{\}.\\

    Un ejemplo de conjuntos de números son los naturales $(\mathbb{N})$, los enteros $(\mathbb{Z})$, los racionales $(\mathbb{R})$ y los reales $(\mathbb{C})$\\

    Los conjuntos se representan entre llaves \{\}, no repiten objetos y no influye el orden. Un conjunto es finito si tiene un número $n$ de elementos llamado \textbf{cardinal}.

    \subsection{Inclusión}
    Dados dos conjuntos $A$ y $B$, se dice que $A$ es \textbf{subconjunto} de $B$ y se denota $A\subseteq B$, si cada elemento de $A$ es también elemento de $B$, si no se escribe $A\not\subseteq B$\\

    Cualquier conjunto siempre admite como subconjunto al conjunto vacío $\emptyset$ y al propio conjunto.\\

    Si $A\subseteq B$ y $A\not=B$ se dice que A está contenido estrictamente en B y se denota $A\subset B$\\

    $\Rightarrow$ \textbf{Ejemplos}:
    \begin{multicols}{2}
        \begin{itemize}
            \item $\emptyset \subseteq \{a,b,c,\{a,b,c\}\}$
            \item $\{a,\textcolor{red}{\{b\}}\} \not\subseteq \{a,b,c,\{a,b,c\}\}$
        \end{itemize}
        \columnbreak
        \begin{itemize}
            \item $\textcolor{Purple}{\{a,b,c\}} \in\{a,b,c,\textcolor{Purple}{\{a,b,c\}}\}$
            \item $\{\textcolor{Purple}{a},\textcolor{Purple}{b}\} \subseteq \{\textcolor{Purple}{a},\textcolor{Purple}{b},c,\{a,b,c\}\}$
        \end{itemize}
    \end{multicols}

    \vspace{8px}
    El conjunto formado por todos los subconjuntos de uno dado $X$, el mismo y el conjunto vacío, se denomina \textbf{conjunto partes de X} o $P(X)$. 
    
    Si $X$ es finito con cardinal $n$, $|P(X)|=2^n$.\\

    Sean $A$ y $X$ dos conjuntos, se verifica que: $\quad A\in P(x) \quad y \quad A\subset X$\\

    \vspace{8px}
    $\Rightarrow$ \textbf{Ejemplos}:
    \begin{itemize}
        \item Si $X=\{a,b\}\ \rightarrow \ P(X)=\{\emptyset,{a},{b}X\}$
        \item $A=\{a,b,c\}\ \rightarrow \ |A|=3 \qquad P(A)=\{\emptyset, \{a\},...,\{a,b\},...,A\}\ \rightarrow\ |P(A)|=2^3=8$
    \end{itemize}

    \vspace{8px}
    Cuando en un contexto determinado consideramos varios conjuntos, estos se considerarán un \textbf{conjunto universal} $U$ o universo.

    \subsection{Operaciones con conjuntos. Propiedades}
    Las operaciones que se definen sobre $P(X)$ se corresponden con las del tema anterior:
    
    \begin{center}
        \begin{tabular}{|c|c|c|}
            \hline
            Boole     & Lógica   & Conjuntos \\ \hline
            $+$       & $\vee$   & $\cup$     \\
            $\cdot$   & $\wedge$ & $\cap$      \\
            $\bar{\ }$& $\neg$   & $\bar{\ \ }$ \\ \hline
        \end{tabular}
    \end{center}

    El complementario $\overline{A}$ está formado por todos los elementos de $U$ que no pertenecen a $A$:


    \begin{center}
        \begin{tikzpicture} [scale=0.6, transform shape]
            \draw[filled] (0,0) rectangle (8,4) node[below left] {$U\ $};
            \draw[outline, fill=White] (3.5,2) circle (1.5cm) node[above right] {$A$};
        \end{tikzpicture}
    \end{center}

    La \textbf{unión} $\cup$ es el conjunto formado por los elementos que pertenecen a A o B o a ambos.\\

    La \textbf{intersección} $\cap$ es el conjunto formado por los elementos que pertenecen a A y B.\\

    \begin{multicols}{2}
        \begin{center}
            \begin{tikzpicture}[scale=0.6, transform shape]
                \draw[filled] (0,0) circle (1.5cm) node {$A$}
                            (0:2cm) circle (1.5cm) node {$B$};
                \node[anchor=south] at (current bounding box.north) {$A \cup B$};
            \end{tikzpicture}
        \end{center}
        \begin{center}
            \columnbreak
            \begin{tikzpicture}[scale=0.6, transform shape]
                \begin{scope}
                    \clip (0,0) circle (1.5cm);
                    \fill[filled] (0:2cm) circle (1.5cm);
                \end{scope}
                \draw[outline] (0,0) circle (1.5cm) node {$A$};
                \draw[outline] (0:2cm) circle (1.5cm) node {$B$};
                \node[anchor=south] at (current bounding box.north) {$A \cap B$};
            \end{tikzpicture}
        \end{center}
    \end{multicols}

    Dos conjuntos son \textbf{disjuntos} cuando no tienen ningún elemento en común: $A\cap B = \emptyset$

    La \textbf{diferencia} entre dos conjuntos $A$ y $B$, representada $A\backslash B$ o $A-B$, es el conjunto de los elementos de A que no pertenecen a B.

    \begin{center}
        \begin{tikzpicture}[scale=0.6, transform shape]
            \draw[filled] (0,0) circle (1.5cm) node {$A$};
            \draw[fill=white, thick, draw=Periwinkle] (0:2cm) circle (1.5cm) node {$B$};
            \draw[dashed, draw=Periwinkle] (0,0) circle (1.5cm) node {$A$};
            \node[anchor=south] at (current bounding box.north) {$A \backslash B$};
        \end{tikzpicture}
    \end{center}

    $\Rightarrow$ \textbf{Ejemplo}: Si $A=\{x,y\}$ y $B=\{x,\{y\}\} \quad$ entonces $\quad A \backslash B = \{y\} \quad$ y $\quad B \backslash A = \{\{y\}\}$\\

    Una \textbf{partición} de $A$ es una colección de subconjuntos $\{A_1, A_2,..., A_n\}$ no vacíos de A que verifica estas propiedades:

    \begin{multicols}{2} 
        \noindent
        \[A=\bigcup_{i=1}^n A_i\]
        \columnbreak
        \[
            \begin{array}{l}
                \ \\ %vertical space
                A_i\cap A_j=\emptyset \text{ para todo } i,j\in n \qquad i\not=j
            \end{array}
        \]
    \end{multicols}

    $\Rightarrow$ \textbf{Ejemplo}: $B=\{a,b,c,2,3,6\}$ una partición sería: $\{A_1,A_2,A_3\}$ dónde:
    \[A_1=\{a,b,c\}\quad A_2=\{2,3\}\quad A_3=\{6\}\]

    \subsection{Propiedades}
    Dados los conjuntos $A,B\subseteq U$ se verifican las siguientes propiedades:
    \begin{multicols}{4} 
        \noindent
        \[i)\ \overline{\emptyset} = U\]
        \columnbreak
        \[ii)\ \overline{U} = \emptyset\]
        \columnbreak
        \[iii)\ \overline{\overline{A}} = A\]
        \columnbreak
        \[iv)\ A\subseteq B \leftrightarrow \overline{A}\subseteq\overline{B}\]
    \end{multicols}

    \begin{multicols}{4} 
        \noindent
        \[v)\ A \cup \emptyset = A\]
        \columnbreak
        \[vi)\ A \cap U = A\]
        \columnbreak
        \[vii)\ A\cap \overline{A}=\emptyset\]
        \columnbreak
        \[viii)\ A\cup \overline{A}=U\]
    \end{multicols}

    La $\cup$ y la $\cap$ son conmutatuvas, asociativas y distributivas respecto a la intersección:

    \begin{itemize}
        \item $A\cup B = B\cup A \qquad y \qquad A\cap B = B\cap A$
        \item $A\cup (B\cup C) = (A\cup B)\cup C \qquad y\qquad A\cap (B\cap C) = (A\cap B)\cap C$
        \item $A\cup (B \cap C) = (A\cup B)\cap(A\cup C) \qquad y\qquad A\cap (B \cup C) = (A\cap B)\cup(A\cap C)$
    \end{itemize}

    \begin{center}
        \setlength{\tabcolsep}{15pt}
        \renewcommand{\arraystretch}{1.3}
        \begin{tabular}{|cl|}
            \hline
            \multicolumn{2}{|c|}{Propiedades de conjuntos} \\ \hline
            \multicolumn{1}{|c|}{Idempotencia} & $ A\cup A = A \hfill A\cap A = A\ $ \\ \hline
            \multicolumn{1}{|c|}{Acotación}    & $ A\cup U = U \hfill A\cap \emptyset = \emptyset\ $ \\ \hline
            \multicolumn{1}{|c|}{Absorción}    & $ A\cup(A\cap B) = A\qquad A\cap(A\cup B) = A\ $ \\ \hline
            \multicolumn{1}{|c|}{De Morgan}    & $ \overline{A\cup B}=\overline{A}\cap\overline{B} \hfill \overline{A\cap B}=\overline{A}\cup\overline{B}\ $ \\ \hline
        \end{tabular}
    \end{center}

    \section{Producto Cartesiano}
    El \textbf{producto cartesiano} de dos conjuntos $A \times B$, es un conjunto por pares ordenads de elementos de la forma $(a,b)$.\\

    Es útil representar gráficamente el producto cartesiano por medio de coordenadas cartesianas. Se puede extender la definición de producto cartesiano a $n$ conjuntos:

    \[A_1\times A_2\times \cdots \times A_n = \{(a_1,a_2,\cdots,a_n)\ |\ a_i\in A_i\}\]

    Finalmente siendo $A_n$ conjuntos finitos se tiene que:
    \[|A_1\times A_2\times \cdots \times A_n|=|A_1|\cdot|A_2|\cdots |A_n|\]

    \newpage
    \section{Aplicaciones. Tipos de aplicaciones}
    Una \textbf{aplicación} $f$ de $A$ en $B$ es una regla que asocia \underline{cada} elemento de $A$ a un \underline{único} elemento de $B$, denominado \textbf{imagen} $f(a)$.\\
    \vspace{-10px}

    \begin{multicols}{2}
        \vspace*{\fill}
        \[
            \begin{array}{l}
                f:\ A\rightarrow B \\
                a\rightarrow f(a)=b
            \end{array}
            \vspace*{20px}
        \]
        \columnbreak
        \begin{tikzpicture}[scale=0.6, transform shape]
            \draw (0,2) node {$A$};
            \draw (0,0) ellipse (1cm and 1.5cm);
            \node at (0,0.9) {$1$}; \node at (0,0) {$2$}; \node at (0,-0.9) {$3$};
            
            \draw (3,2) node {$B$};
            \draw (3,0) ellipse (1cm and 1.5cm);
            \node at (3,0.9) {$x$}; \node at (3,0.3) {$y$}; \node at (3,-0.3) {$z$};  \node at (3,-0.9) {$t$};

            \draw (0.2,0.9) [-to] to (2.8,0.9);
            \draw (0.2,0) to (1.3,0.14) node[above, color=red]{$\quad \text{no!}$} [-Rays, color=red] to (2.8,0.3);
            \draw (0.2,0) [-to] to (2.8,-0.25);
            \draw (0.2,-0.9) [-to] to (2.8,-0.35);
        \end{tikzpicture}
    \end{multicols}

    Sea $f: A \rightarrow B$  y $A_1\subseteq A$ y $B_1\subseteq B$. Se define imagen por $f$ del conjunto $A_1$ como:

    \[f_{\ast}(B_1)=\{f(a)\ |\ a\in A_1\}\subseteq B\]

    Y la imagen recíproca por f del conjunto B como:
    \[f^\ast=\{a\in A\ |\ f(a)\in B_1\}\subseteq A\]

    Si tomamos $A_1=A$, el conjunto $f_\ast(A)=Im(f)$ es el conjunto imagen.\\

    $\Rightarrow$ \textbf{Ejemplo:} Sea $f:\ A=\{1,2,3\}\rightarrow B=\{x,y,z,t\}$ la aplicación representada en el diagrama anterior $\quad \ f(1)=x \quad f(2)=f(3)=z $:

    \[f_*(\{1\})=\{x\} \qquad f_*(\{2,3\})=\{z\}\qquad f_*(\{1,2\})=\{x,z\}\]
    \[f^*(\{x,y\})=\{1\} \qquad f^*(\{x,z\})=\{1,2,3\}=A \qquad f^*(\{t,y\})=\emptyset\]

    \subsection{Tipos de apliaciones}

    \begin{multicols}{2}
        \begin{itemize}
            \item Una aplicación es \textbf{inyectiva} si elementos distintos de A tienen imágenes diferentes de B.
        \end{itemize}
        \[[a_1\not=a_2\rightarrow f(a_1)\not = f(a_2)]\]
        \columnbreak
        \begin{center}
            \begin{tikzpicture}[scale=0.6, transform shape]
                \draw (0,2) node {$A$};
                \draw (0,0) ellipse (1cm and 1.5cm);
                \node at (0,0.9) {$1$}; \node at (0,0) {$2$}; \node at (0,-0.9) {$3$};
                
                \draw (3,2) node {$B$};
                \draw (3,0) ellipse (1cm and 1.5cm);
                \node at (3,0.9) {$x$}; \node at (3,0.3) {$y$}; \node at (3,-0.3) {$z$};  \node at (3,-0.9) {$t$};
    
                \draw (0.2,0.9) [-to] to (2.8,0.9);
                \draw (0.2,0) [-to] to (2.8,-0.3);
                \draw (0.2,-0.9) [-to] to (2.8,-0.9);
            \end{tikzpicture}
        \end{center}
    \end{multicols}

    \begin{multicols}{2}
        \begin{itemize}
            \item Una aplicación es \textbf{sobreyectiva} si todo elemento de B es imagen de A.
        \end{itemize}
        \[\forall\ b \in B,\ \exists\ a\in A\quad [b=f(a)]\]
        \columnbreak
        \begin{center}
            \begin{tikzpicture}[scale=0.6, transform shape]
                \draw (0,2) node {$A$};
                \draw (0,0) ellipse (1cm and 1.5cm);
                \node at (0,0.9) {$1$}; \node at (0,0.3) {$2$}; \node at (0,-0.3) {$3$};
                \node at (0,-0.9) {$4$};
    
                \draw (3,2) node {$B$};
                \draw (3,0) ellipse (1cm and 1.5cm);
                \node at (3,0.9) {$x$}; \node at (3,0) {$y$}; \node at (3,-0.9) {$z$};
    
                \draw (0.2,0.9) [-to] to (2.8,0.97);
                \draw (0.2,0.3) [-to] to (2.8,0);
                \draw (0.2,-0.3) [-to] to (2.8,0.8);
                \draw (0.2,-0.9) [-to] to (2.8,-0.9);
            \end{tikzpicture}
        \end{center}
    \end{multicols}

    \begin{multicols}{2}
        \begin{itemize}
            \item Una aplicación es \textbf{biyectiva} si es inyectiva y sobreyectiva (mismo cardinal).
        \end{itemize}
        \columnbreak
        \begin{center}
            \begin{tikzpicture}[scale=0.6, transform shape]
                \draw (0,2) node {$A$};
                \draw (0,0) ellipse (1cm and 1.5cm);
                \node at (0,0.9) {$1$}; \node at (0,0) {$2$}; \node at (0,-0.9) {$3$};
    
                \draw (3,2) node {$B$};
                \draw (3,0) ellipse (1cm and 1.5cm);
                \node at (3,0.9) {$x$}; \node at (3,0) {$y$}; \node at (3,-0.9) {$z$};
    
                \draw (0.2,0.9) [-to] to (2.8,0.9);
                \draw (0.2,0) [-to] to (2.8,0);
                \draw (0.2,-0.9) [-to] to (2.8,-0.9);
            \end{tikzpicture}
        \end{center}
    \end{multicols}

    $\Rightarrow$ \textbf{Ejercicio 20:} Sea $A = \{a,b,c,d,e,k\}$, $B=\{d,e\}$, $C_1=\{\{a,d\},\emptyset\}$ y $Y_1=\{2,6\}$:\\
    \[
        \begin{array}{l}
            f: \mathcal{P}(A)\rightarrow \mathbb{N}\\
            X \rightarrow f(X)=|X\cup B| \qquad \qquad \qquad \text{Calcula:}\\
            \
        \end{array}
    \]
    \begin{multicols}{2}
        \begin{itemize}
            \item $f(\emptyset)=|\emptyset\cup \{d,e\}|=3$
            \item $f(\{b,c\})=|\{b,c\}\cup\{d,e\}|=4$
            \item $f_\ast(C_1)= \{f_\ast(\{a,d\})\cup f_\ast (\emptyset)\}=\{3,2\}$
            \item $f^\ast(Y_1)=\{f^\ast(\{2\})\cup f^\ast(\{6\})\}=\{B,\emptyset,\{d\},\{e\}\}\cup\{A,\{a,b,c,k\},...\}$
        \end{itemize}
    \end{multicols}

    \section{Composición de aplicaciones. Aplicación inversa}

    Dados $A,\ B,\ C$ tres conjuntos y dos aplicaciones f y g tal que:
    \[f:A\rightarrow B \qquad g:B\rightarrow C\]
    se llama \textbf{composición de f con g} a la aplicación:


    \begin{center}
        \begin{tikzpicture} [scale=0.8]
            \node at (2,2) {$\boxed{g\circ f:A\rightarrow C}$};
            \node at (0,0) {A};
            \node at (1,0.3) {$f$};
            \draw (0.5,0)  [-to] to (1.5,0);
            \node at (2,0) {B};
            \node at (3,0.3) {$g$};
            \draw (2.5,0)  [-to] to (3.5,0);
            \node at (4,0) {C};
            \node at (2,1) {$g\circ f$};
            \draw (0,0.5)  [-to, bend left=10] to (4,0.5);

            \begin{scope}[yshift=-0.8cm]
                \node at (0,0) {$a$};
                \draw (0.5,0)  [-to] to (1.5,0);
                \node at (2,0) {$f(a)$};
                \draw (2.5,0)  [-to] to (3.5,0);
                \node at (4.3,0) {g(f(a))};
            \end{scope}

            \begin{scope} [xshift=8cm, yshift=0.9cm, scale=0.8, transform shape]
                \draw (0,2) node {$A$};
                \draw (0,0) ellipse (1cm and 1.5cm);
                \node at (0,0.9) {$1$}; \node at (0,0) {$2$}; \node at (0,-0.9) {$3$};
    
                \draw (3,2) node {$B$};
                \draw (3,0) ellipse (1cm and 1.5cm);
                \node at (3,0.9) {$1$}; \node at (3,0) {$2$}; \node at (3,-0.9) {$3$};

                \draw (6,2) node {$C$};
                \draw (6,0) ellipse (1cm and 1.5cm);
                \node at (6,0.9) {$1$}; \node at (6,0) {$2$}; \node at (6,-0.9) {$3$};
    
                \draw (0.2,0.9) [-to] to (2.8,1);
                \draw (0.2,0) [-to] to (2.8,0.9);
                \draw (0.2,-0.9) [-to] to (2.8,0.8);

                \draw (3.2,0.9) [-to] to (5.8,-0.9);
                \draw (3.2,0) [-to] to (5.8,0.9);
                \draw (3.2,-0.9) [-to] to (5.8,0);

                \node at (3,-2.2) {$(g\circ f)(a) = 5\ $ y $\ (f\circ g)(a) = 1$};
            \end{scope}
        \end{tikzpicture}
    \end{center}

    Se verifica que si $f$ y $g$ son inyectivas, sobreyectivas o biyectivas, también lo será $(g\circ f)$.\\

    Se llama \textbf{identidad} a una apliación de la forma $I_A:\ A\rightarrow A$ (deja la salida igual). Es inmediato comprobar que cualquier aplicación $f: A\rightarrow B$ verifica:
    \[(f\circ I_A)=f=(I_B\circ f)\]

    Se llama \textbf{aplicación inversa} $f^{-1}$ a una aplicación $f^{-1}: B \rightarrow A\ $ tal que:
    \[b=f(a)\ \longrightarrow\ f^{-1}(b)=a\]

    Una aplicación $f:A\rightarrow B$ \textbf{admite inversa }si, y solo si, f \textbf{es biyectiva}.

    \begin{itemize}
        \item Una aplicación es inversa por la izquierda si $g\circ f = Id_A$
        \item Una aplicación es inversa por la derecha si $g\circ f = Id_B$
    \end{itemize}
    \vspace{7px}
    \begin{center}
        \begin{tikzpicture} [scale=0.8, transform shape]
            \node at (3,3) {$\Rightarrow$\textbf{EJ 23}: Define una inversa por izquierda para $f$:};

            \draw (0,2) node {$A$};
            \draw (0,0) ellipse (1cm and 1.5cm);
            \node at (0,0.9) {$1$}; \node at (0,0) {$2$}; \node at (0,-0.9) {$3$};

            \draw (3,2) node {$B$};
            \draw (3,0) ellipse (1cm and 1.5cm);
            \node at (3,0.9) {$x$}; \node at (3,0.3) {$y$}; \node at (3,-0.3) {$z$};
            \node at (3,-0.9) {$t$};

            \draw (6,2) node {$A$};
            \draw (6,0) ellipse (1cm and 1.5cm);
            \node at (6,0.9) {$1$}; \node at (6,0) {$2$}; \node at (6,-0.9) {$3$};

            \draw (0.3,2) [-to] to (2.7,2);
            \node at (1.5,2.3){$f$};

            \draw (0.2,0.9) [-to] to (2.8,0.9);
            \draw (0.2,0) [-to] to (2.8,0.-0.3);
            \draw (0.2,-0.9) [-to] to (2.8,0.3);

            \draw (3.3,2) [-to, color=Periwinkle] to (5.7,2);
            \node at (4.5,2.3)[color=Periwinkle]{$f^{-1}$};

            \draw (3.2,0.9) [-to,color=Periwinkle] to (5.8,0.9);
            \draw (3.2,0.3) [-to,color=Periwinkle] to (5.8,-0.9);
            \draw (3.2,-0.3) [-to,color=Periwinkle] to (5.8,0);
            \draw (3.2,-0.9) [-to,color=Periwinkle,dashed] to (5.8,-1) node[below left]{\scriptsize a cualquiera$\quad\ $};
            \draw (3.2,-0.9) [-to,color=Periwinkle,dashed] to (4.3,-0.5);
            \draw (3.2,-0.9) [-to,color=Periwinkle,dashed] to (3.8,-0.4);

            \begin{scope}[xshift=10cm]
                \node at (3,3) {$\Rightarrow$\textbf{EJ 24}: Define una inversa por derecha para $f$:};

                \draw (0,2) node {$A$};
                \draw (0,0) ellipse (1cm and 1.5cm);
                \node at (0,0.9) {$1$}; \node at (0,0) {$2$}; \node at (0,-0.9) {$3$};
    
                \draw (3,2) node {$B$};
                \draw (3,0) ellipse (1cm and 1.5cm);
                \node at (3,0.9) {$x$}; \node at (3,0.3) {$y$}; \node at (3,-0.3) {$z$};
                \node at (3,-0.9) {$t$};
    
                \draw (6,2) node {$A$};
                \draw (6,0) ellipse (1cm and 1.5cm);
                \node at (6,0.9) {$1$}; \node at (6,0) {$2$}; \node at (6,-0.9) {$3$};
    
                \draw (3.3,2) [-to, ] to (5.7,2);
                \node at (4.5,2.3)[]{$f$};

                \draw (3.2,0.9) [-to] to (5.8,0.9);
                \draw (3.2,0.3) [-to] to (5.8,0.0);
                \draw (3.2,-0.3) [-to] to (5.8,-0.8);
                \draw (3.2,-0.9) [-to] to (5.8,-0.9);
    
                \draw (0.3,2) [-to,color=Peach] to (2.7,2);
                \node at (1.5,2.3)[color=Peach]{$f^{-1}$};

                \draw (0.2,0.9) [-to, color=Peach] to (2.8,0.9);
                \draw (0.2,0) [-to, color=Peach] to (2.8,0.3);
                \draw (0.2,-0.8)[-to, color=Peach, dashed] to (2.8,-0.3);
                \draw (0.2,-0.9)[-to, color=Peach, dashed] to (2.8,-0.9) node [below left]{\scriptsize hay dos $\scriptstyle f^{-1}$ dcha};
            \end{scope}
        \end{tikzpicture}
    \end{center}

\end{document}