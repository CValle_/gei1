\documentclass[a4paper, 12pt, titlepage]{extarticle}
\newcommand\tab[1][1cm]{\hspace*{#1}}
\usepackage[utf8]{inputenc}
\usepackage[spanish]{babel}
\usepackage{fancyhdr}

\setlength{\parindent}{0pt}
\usepackage[margin=1in,bottom=1.2in,top=1.4in]{geometry}

\usepackage{amsmath, mathtools, amssymb, amsfonts, mathrsfs, array} %math
\usepackage{multicol}
\usepackage{pifont}
\usepackage{enumitem}

\usepackage{hyperref} %links
\usepackage{soul}
\usepackage[dvipsnames]{xcolor}
\hypersetup{
    colorlinks,
    linkcolor={black},
    citecolor={blue!50!black},
    urlcolor={blue!80!black}
}
\definecolor{Lilac}{HTML}{c3cde6}

\newcommand{\hllc}[1]{\colorbox[HTML]{c3cde6}{$\displaystyle #1$}}
\newcommand{\hllr}[1]{\colorbox[HTML]{ffd7c6}{$\displaystyle #1$}} % formato ancho rosa
\newcommand{\hlfancy}[2]{\sethlcolor{#1}\hl{#2}}

\usepackage{tikz}
\usetikzlibrary{tikzmark}
\usepackage[siunitx]{circuitikz}
\usepackage{kvmap}

\newcommand{\xmark}{\ding{55}}
\newcommand{\enter}{\vspace{10px}}

%--- MAKETITLE ------------------------------------------------------------------------------

\def\myauthor{Clara Valle Gómez}
\author{\myauthor}

\title{
    \begin{Huge}
        {\textbf{MATEMÁTICA DISCRETA}}\\
    \end{Huge}
    \vspace{50px}
    \textbf{Tema I}\\
    Lógica y álgebras de boole
    \date{\today}
}

%--- CABECERA Y PIE DE PÁGINA -------------------------------------------------------------------------
\setlength{\headheight}{16.00pt} %warning
\pagestyle{fancy}
\lhead{Tema 1}
\chead{}
\rhead{C.Valle} 
\lfoot{}
\cfoot{}
\rfoot{\thepage}    

\begin{document}
\maketitle

    
%--- ÍNDICE -----------------------------------------------------------
    \newpage
    \tableofcontents

%--- DOC ---------------------------------------------------------- 
    \newpage
    \section{Lógica proposicional}
    Una proposición es una oración declarativa que es \textbf{verdadera (1) o falsa (0)}.\\

    Las proposiciones primitivas no se pueden descompober en otras más simples. Para obtener proposiciones compuestas usamos operadores lógicos: $\neg, \vee, \wedge, \rightarrow, \leftrightarrow$

    \begin{multicols}{2}
        Negación $\neg$
        \\
        \columnbreak

        ``no p''\\
        ``no es cierto p''
    \end{multicols}
    \begin{multicols}{2}
        Conjunción $\wedge$ {\scriptsize\textit{(and)}}
        \\
        \columnbreak

        ``p y q''\\
        ``p, q''\\
        ``p, pero q''\\
    \end{multicols}
    \begin{multicols}{2}
        Disyunción $\vee$ {\scriptsize\textit{(or)}}
        \\
        \columnbreak

        ``p o q (o ambos)''\\
        ``como mínimo p o q''
    \end{multicols}
    \vspace{4px}
    \begin{multicols}{3}
        Condicional $\rightarrow$
        \\
        \columnbreak

        ``si p entonces q''\\
        ``si p, q'' \\
        ``si p, solo si q'' \\
        ``cuando p, entonces q'' \\
        ``p es suficiente para q'' \\
        \columnbreak

        ``q si p''\\
        ``q siempre que p''\\
        ``q cuando p''\\
        ``q es necesario para p''\\
    \end{multicols}

    \begin{multicols}{2}
        Bicondicional $\leftrightarrow$
        \\
        \columnbreak

        ``p si y solo si q''\\
        ``p es suficiente y necesario para q''\\
    \end{multicols}

    Existe una jerarquía de prioridades: negación, conjunción$/$disyunción y condicional$/$bicondicional.\\

    $\Rightarrow$ \textbf{Ejercicio 1:} Sean las siguientes proposiciones formaliza los enunciados:
    \begin{center}
        \begin{multicols}{2}
            h: Salgo hoy\\
            f: Voy a la fiesta
            \columnbreak
    
            c: Voy al concierto\\
            d: Duermo en casa
        \end{multicols}
    \end{center}

    \begin{enumerate}[label=\alph*)]
        \item Salgo hoy, pero no voy al concierto \hspace{91px} $h\wedge \neg c $
        \item Si salgo hoy, voy al concierto pero no a la fiesta \hspace{26px} $h \rightarrow c \wedge \neg f  $
        \item Salgo hoy, si y solo si, voy a la fiesta o al concierto \hspace{10px} $h \leftrightarrow f \vee c $
        \item Salgo hoy solo si voy a la fiesta \hspace{110px} $h \rightarrow f$
        \item Duermo en casa si no salgo hoy \hspace{109px} $\neg h \rightarrow d$
        \item Si no $d$, entonces $c$ solo si no $f$ \hspace{112px} $\neg d \rightarrow (c\leftrightarrow \neg f)$
    \end{enumerate}

    \subsection{Operadores Lógicos. Semántica}
    \enter
    \begin{center}
        \setlength{\tabcolsep}{9pt}
        \renewcommand{\arraystretch}{1.2}
        \begin{tabular}{|c|c|c|c|c|c|c|}
            \hline
            $p$ & $q$ & $\neg p$ & $p \vee q$ & $p \wedge q$ & $p \rightarrow q$ & $p \leftrightarrow q$ \\ \hline
            0   & 0   & 1        & 0            & 0          & 1                 & 1                     \\ \hline
            0   & 1   & 1        & 1            & 0          & 1                 & 0                     \\ \hline
            1   & 0   & 0        & 1            & 0          & 0                 & 0                     \\ \hline
            1   & 1   & 0        & 1            & 1          & 1                 & 1                     \\ \hline
            \end{tabular}
    \end{center}

    Una \textbf{interpretación} es el resultado de las proposiciones simples de $\mathcal{P}$, es decir, cada fila. Si en $\mathcal{P}$ intervienen $n$ proposiciones primitivas, hay $2^n$ posibles interpretaciones.\\

    Si el resultado de una interpretación de $\mathcal{P}$ es (1) es un \textbf{modelo}, si es (0) es un \textbf{contraejemplo}.\\

    Una fórmula que siempre es verdadera es una \textbf{tautología} $(\top)$ una fórmula que siempre es falsa es una \textbf{contradición} $(\perp)$, si no es ninguna de las anteriores es una \textbf{contingencia}.\\

    Un conjunto de proposiciones $\mathcal{P}$ es \textbf{consistente o satisfacible} si admite al menos un modelo, el caso contrario se dice inconsistente.\\


    $\Rightarrow$ \textbf{Ejercicio 4:} Construye las tablas de verdad e indica si es $\top,\ \perp$ o contingencia:
    \begin{enumerate} [label=\alph*)]
        \item $\neg p \vee q \rightarrow (r \rightarrow \neg p)$
    \end{enumerate}

    \begin{center}
        \begin{tabular}{|c|c|c|c|c|c|c|}
            \hline
            $p$ & $q$ & $r$ & $\neg p$ & $\neg p \vee q$ & $r \rightarrow \neg p$ & $\neg p \vee q \rightarrow (r\rightarrow \neg p)$ \\ \hline
            0   & 0   & 0   & 1        & 1               & 1                      & \textbf{1}                                               \\ \hline
            0   & 0   & 1   & 1        & 1               & 1                      & \textbf{1}                                               \\ \hline
            0   & 1   & 0   & 1        & 1               & 1                      & \textbf{1}                                               \\ \hline
            0   & 1   & 1   & 1        & 1               & 1                      & \textbf{1}                                               \\ \hline
            1   & 0   & 0   & 0        & 0               & 1                      & \textbf{1}                                               \\ \hline
            1   & 0   & 1   & 0        & 0               & 0                      & \textbf{1}                                               \\ \hline
            1   & 1   & 0   & 0        & 1               & 1                      & \textbf{1}                                               \\ \hline
            1   & 1   & 1   & 0        & 1               & 0                      & \textbf{0}                                               \\ \hline
        \end{tabular}
        \hspace{10px} \textbf{Es una contingencia}
    \end{center}
    \begin{enumerate} [label=b)]
        \item $(p \wedge q) \wedge \neg(p \vee\neg q) \quad$ es equivalente a $\quad (p \wedge q) \wedge (\neg p \wedge q)$
    \end{enumerate}
    \begin{center}
        
    \end{center}
    \hspace{10.2px}
    \begin{tabular}{|c|c|c|c|c|c|}
        \hline
        $p$ & $q$ & $\neg p$ & $p \wedge q$ & $\neg p \wedge q$ & $ (p \wedge q)\wedge (\neg p \wedge q)$ \\ \hline
        0   & 0   & 1        & 0            & 0                 & \textbf{0}                                       \\ \hline
        0   & 1   & 1        & 0            & 0                 & \textbf{0}                                       \\ \hline
        1   & 0   & 0        & 0            & 0                 & \textbf{0}                                       \\ \hline
        1   & 1   & 0        & 1            & 0                 & \textbf{0}                                       \\ \hline
    \end{tabular}
    \hspace{10px} \textbf{Es una contradicción}

    \subsection{Implicaciones y equivalencias lógicas}

    Dos proposiciones $\mathcal{P}$ y $\mathcal{Q}$ son \textbf{lógicamente equivalentes} si $\mathcal{P} \leftrightarrow \mathcal{Q}$ es una tautología.

    Se representa por $\mathcal{P}\equiv \mathcal{Q}$ o $\mathcal{P} \Leftrightarrow \mathcal{Q}$\\

    También se dice que $\mathcal{P}$ \textbf{implica lógicamente} $\mathcal{Q}$ cuando $\mathcal{P} \rightarrow \mathcal{Q}$ es una tautología.

    Se representa por $\mathcal{P}\models \mathcal{Q}$ o $\mathcal{P} \Rightarrow \mathcal{Q}$\\

    \begin{center}
        \setlength{\tabcolsep}{15pt}
        \renewcommand{\arraystretch}{1.2}
    
        \begin{tabular}{|cl|}
            \hline
            \multicolumn{2}{|c|}{Principales equivalencias lógicas}                                          \\ \hline
            \multicolumn{1}{|c|}{Doble Negación}  &  $\neg \neg p \equiv p$                                  \\ \hline
            \multicolumn{1}{|c|}{Conmutativa}     & $ \begin{array}{lcl} p\vee q\equiv q \vee p \\ p\wedge q\equiv q \wedge p \end{array}$ \\ \hline
            \multicolumn{1}{|c|}{Asociativa}      & $ \begin{array}{lcl} (p\vee q) \vee r\equiv p \vee (q \vee r) \\ (p\wedge q) \wedge r\equiv p \wedge (q \wedge r) \end{array}$ \\ \hline
            \multicolumn{1}{|c|}{Distributiva}    & $ \begin{array}{lcl} p\vee (q \wedge r)\equiv (p \vee q) \wedge (p \vee r) \\ p\wedge (q \vee r)\equiv (p \wedge q) \vee (p \wedge r) \end{array}$ \\ \hline
            \multicolumn{1}{|c|}{DeMorgan}        & $ \begin{array}{lcl} \neg(p \vee q)\equiv (\neg p \wedge \neg q) \\ \neg(p \wedge q)\equiv (\neg p \vee \neg q)\end{array}$ \\ \hline
            \multicolumn{1}{|c|}{Contraposición}  & $(p \rightarrow q) \equiv (\neg q \rightarrow \neg p)$   \\ \hline
            \multicolumn{1}{|c|}{Implicación}     & $ \begin{array}{lcl} (p\rightarrow q)\equiv (\neg p \vee q) \\ \neg (p\rightarrow q)\equiv (p \wedge \neg q) \end{array}$ \\ \hline
            \multicolumn{1}{|c|}{Equivalencia}    &                                                          \\ \hline
            \multicolumn{1}{|c|}{Idempotentes}    & $ \begin{array}{lcl} p \equiv (p \vee p) \\ p\equiv (p \wedge p) \end{array}$ \\ \hline
            \multicolumn{1}{|c|}{Absorción}       & $ \begin{array}{lcl} p \wedge (p\vee q) \equiv p \\ p \vee (p\wedge q) \equiv p \end{array}$ \\ \hline
            \multicolumn{1}{|c|}{Dominación}      &                                                          \\ \hline
            \multicolumn{1}{|c|}{Identidad}       &                                                          \\ \hline
            \multicolumn{1}{|c|}{Contraposición}  &                                                          \\ \hline
            \end{tabular}

            \vspace{20px}

            \setlength{\tabcolsep}{24pt}
            \renewcommand{\arraystretch}{1.2}

            \begin{tabular}{|cl|}
                \hline
                \multicolumn{2}{|c|}{Principales implicaciones lógicas} \\ \hline
                \multicolumn{1}{|c|}{Modus Ponens}                & $[( p \rightarrow q \wedge p)]\models q$        \\ \hline
                \multicolumn{1}{|c|}{Modus Tollens}               & $[(p\rightarrow q)\wedge \neg q]\models \neg p$ \\ \hline
                \multicolumn{1}{|c|}{Leyes de simplificación}     &     \\ \hline
                \multicolumn{1}{|c|}{Leyes de adición}            &     \\ \hline
                \multicolumn{1}{|c|}{Silogismo Disyuntivo}        &     \\ \hline
                \multicolumn{1}{|c|}{Ley de Casos}                &     \\ \hline
                \multicolumn{1}{|c|}{Ley de inconsistencia}       &     \\ \hline
            \end{tabular}
    \end{center}

    \subsection{Tablas semánticas}
    Permite demostrar si un conjunto de fórmulas es consistente o no donde los conectores se representan por:\\

    \begin{tikzpicture}
        \node {$p\vee q$}
            child {node {$p$}}
            child {node {$q$}};

        \begin{scope}[xshift=4cm, yshift=0.5cm]
            \node {$p\wedge q$}
            child {node {$p$}
                child {node {$q$}}};
        \end{scope}

        \begin{scope}[xshift=9cm]
            \node {$p\rightarrow q$}
            child {node {$\neg p$}}
            child {node {$q$}};
        \end{scope}

        \begin{scope}[xshift=14cm, yshift=0.5cm]
            \node {$p\leftrightarrow q$}
            child {node {$p$}
                child {node {$q$}}
            }
            child {node {$\neg p$}
                child {node {$\neg q$}}
            };
        \end{scope}
    \end{tikzpicture}

    Se contstruye la tabla semántica a partir de descomponer una a una las proposiciones compuestas. Si en un \textbf{camino del árbol aparece una proposición y su negación}, es un \textbf{camino cerrado} ($\ast$).
    
    Si al final todos los caminos se cierran es una contradicción (el conjunto es inconsistente).

    Si al contrario cada camino queda abierto es una tautología (el conjunto es consistente).\\

    $\Rightarrow$ \textbf{Ejercicio 8:} Determina si los siguientes conjuntos son satisfacibles o no.

    \begin{enumerate} [label=\alph*)]
        \item $\{ \neg q \rightarrow \neg r, \ (\neg q\rightarrow p)\rightarrow q,\ \neg r \wedge p,\ s\rightarrow \neg q \}$
    \end{enumerate}

    \[
        \begin{array}{l}
            (\neg q \rightarrow \neg r)\ \textcolor{Peach}{\wedge}\ ((\neg q\rightarrow p)\rightarrow q) \ \textcolor{Peach}{\wedge}\ (\neg r \wedge p)\ \textcolor{Peach}{\wedge}\ (s\rightarrow \neg q)\\

            Implicacion\ \ \neg (\neg q \rightarrow p) \vee q\\
            Implicacion \hspace{13px} (\neg q \wedge \neg p) \vee q\\
            \\
            \underset{4}{\underline{(\neg q \rightarrow \neg r)}}\ 
            \textcolor{Peach}{\wedge}\ 
            \underset{2}{\underline{(\neg q \wedge \neg p) \vee q}} \ 
            \textcolor{Peach}{\wedge}\ 
            \underset{1}{\underline{(\neg r \wedge p)}}\ 
            \textcolor{Peach}{\wedge}\ 
            \underset{3}{\underline{(s\rightarrow \neg q)}}\\
        \end{array}
    \]

    \begin{center}
        \begin{tikzpicture}
            [level 1/.style = {sibling distance = 3cm, level distance = 1.2cm}]

            \node {$\neg r$}
            child {node [red] {$p$}
                child {node {$(\neg q \wedge \neg p) \vee q$}
                    child {node {$(\neg q \wedge \neg p)$}
                        child {node {$\neg q$}
                            child {node [red] {$\neg p$}
                            edge from parent node [red, below, yshift=-21px] {$\scriptstyle \ast$}
                            }
                        }
                    }
                    child {node [Maroon]{$q$}
                        child {node {$\neg s$}
                            child {node {$\neg q \rightarrow \neg r$}
                                child {node {$q$}}
                                child {node {$\neg r$}}
                            }
                        }
                        child {node [Maroon] {$\neg q$}
                        edge from parent node [Maroon, below, xshift=25px, yshift=-21px] {$\scriptstyle \ast$}
                        }
                    }
                }
            };
        \end{tikzpicture}
    \end{center}
    Sería consistente y habría dos modelos $\{q,\neg s, p, \neg r\}$ y $\{\neg r, \neg s, q, p\}$

    \subsection{Argumentos y métodos de demostración}

    Dadas hipótesis $H_n$, la conclusión $C$ es consecuencia de ellas si $H_1 \wedge H_2 \wedge ... \wedge H_n \models C$,es decir, si cuando todas las hipótesis son ciertas también lo es la conclusión (tautologías).\\

    Se pueden realizar \textbf{demostraciones directas mediante tablas de verdad}.\\

    También \textbf{demostraciones directas mediante sucesión de proposiciones} válidas como: Es una de las hipótesis, es una tautología conocida, es lógicamente equivalente a una proposición anterior...\\

    Se pueden realizar \textbf{demostraciónes indirectas por contraposición}, que consiste en demostrar que negar la conclusión equivale a negar las hipótesis:
    \[\neg C \models \ \neg(H_1,H_2,...,H_n) \quad \text{\ equivalentenemente\ } \quad C\models \neg H_1, \neg H_2,..., \neg H_n\]\\

    $\Rightarrow$ \textbf{Ejemplo}: Si a y b son números naturales y $a+b\geq 25$ entonces $a\geq 13$ o $b \geq 13$:\\

    Proposiciones primitivas $p: a\geq 13;\ q: b \geq 13;\ r: a+b\geq 25$\\

    Queremos ver que $r\models p \vee q$, para ello veremos si $\neg r\models \neg(p \vee q)$\\

    \begin{itemize}
        \item Apicamos leyes de De Morgan: $\neg(p \vee q)\equiv (\neg p \wedge \neg q)$
    \end{itemize}

    $\quad$ Ahora con $ \quad \neg p \wedge \neg q \quad$ que es $\quad a \leq 12$ y $b \leq 12 \quad$ se cumple $\quad a+b\leq 24\ (\neg r)$\\

    Por lo tanto $r \models p\vee q \ \checkmark$\\
    \vspace{7px}

    Y por último \textbf{demostraciones indirectas por reducción al absurdo} o contradicción, que consiste en probar que:
    \[
        \begin{array}{c}
            \text{El argumento } \{H_1,H_2,...,H_n\}\models C \text{ es válido, si y solo si, el conjunto }\\
            \{H_1,H_2,...,H_n,\neg C\} \text{ es inconsistente (no tiene ningún modelo).}
        \end{array}
    \]
    Por tablas semánticas debemos demostrar que $H_1 \wedge H_2 \wedge... \wedge H_n \wedge \neg C$ es una contradicción.\\

    $\Rightarrow$ \textbf{Ejemplo}: Demostrar Modus Tollens $[(p\rightarrow q)\wedge \neg q]\models \neg p$

    \[(p\rightarrow q) \ \textcolor{Peach}{\wedge}\ \neg q  \ \textcolor{Peach}{\wedge}\ \neg(\neg p) \ \equiv \ (p\rightarrow q) \ \textcolor{Peach}{\wedge}\ \neg q  \ \textcolor{Peach}{\wedge}\ p\]
    \begin{center}
        \begin{tikzpicture}
            [level 1/.style = {level distance = 1.1cm}]

            \node {$\neg q$}
            child {node {$p$}
                child {node {$\underset{\ast}{\neg p}$}}
                child {node {$\underset{\ast}{q}$}}
            };
        \end{tikzpicture}
    \end{center}
    El argumento es válido ya que todas las ramas se cierran (contradicción)

    \section{Lógica de predicados}
    Un predicado es una afirmación que expresa una propiedad o una relación entre objetos y su valor depende del que tome una variable de un conjunto llamado dominio. Un predicado se transforma en una proposición cuando sustituyes todas las variables por elementos del dominio o con cuantificadores:

    \begin{itemize}
        \item Cuantificador universal $\forall$
    \end{itemize}
    Este tipo de proposiciones es verdadera cuando $p(a)$ es verdadera para cualquier valor de verdad del dominio $U$ y es falsa si $p(a)$ es falsa para algún valor de $U$.
    \begin{itemize}
        \item Cuantificador existencial $\exists$
    \end{itemize}
    Esta proposición es verdadera cuando $p(a)$ es verdadera para al menos un valor de $U$, es falsa cuando para todo valor de $U$, la proposición $p(a)$ es falsa.\\

    Las \textbf{leyes de De Morgan Generalizadas} son ciertas en cualquier universo y cualquier valor de las proposiciones
    \[\neg[\forall x\ p(x)]\ \equiv\ \exists x\ \neg p(x) \qquad \qquad \neg[\exists x\ p(x)]\ \equiv \ \forall x \neg p(x)\]

    \subsection{Argumentos en lógica de predicados}
    \begin{itemize}
        \item \textbf{Especificación universal(EU)} Si $\forall x F(x)$ es verdad, se puede deducir que F(a) es verdad para cualquier elemento a del dominio.
        \begin{center}
            \begin{tikzpicture}
                [level 1/.style = {level distance = 1.15cm}]
                \node {$\forall x\ F(x)$}
                child {node {$F(a)$}};
            \end{tikzpicture}
        \end{center}
        \item \textbf{Especificación existencial(EE)} Si la proposición $\exists x F(x)$ es verdad entonces existe un elemento $a$ para el que $F(a)$ es verdad. Debe marcarse como utilizada y el mobre de la variable debe ser nuevo en cada rama.
        \begin{center}
            \begin{tikzpicture}
                [level 1/.style = {level distance = 1.15cm}]
                \node {$\exists x\ F(x)\ \checkmark$}
                child {node {$F(a)$}};
            \end{tikzpicture}
        \end{center}
    \end{itemize}
    \textbf{Se debe aplicar la Especificación Existencial antes de la Universal}\\

    $\Rightarrow$ \textbf{Ejercicio 20}: Por tablas semánticas, prueba que el siguiente argumento es válido:
    \[\forall x\ (P(x)\rightarrow Q(x)),\ \forall y\ (Q(y)\rightarrow R(y))\ \models \ (\forall x\ P(x))\rightarrow (\forall x\ R(x))\]
    Por reducción al absurdo:
    \[
        \begin{array}{l}
            \forall x\ (P(x)\rightarrow Q(x))\ \textcolor{Peach}{\wedge}\ \forall y\ (Q(y)\rightarrow R(y))\ \textcolor{Peach}{\wedge}\ \neg((\forall x\ P(x))\rightarrow (\forall x\ R(x)))\\
            \hspace{168px} implicacion\ \ (\forall x\ P(x))\wedge \neg(\forall x\ R(x))\\
            \hspace{164px} Morgan\ gen\ \ (\forall x\ P(x))\wedge (\exists x\ \neg\ R(x))
        \end{array}
    \]
    \\
    \[\forall x\ (P(x)\rightarrow Q(x))\ \textcolor{Peach}{\wedge}\ \forall y\ (Q(y)\rightarrow R(y))\ \textcolor{Peach}{\wedge}\ (\forall x\ P(x))\wedge (\exists x\ \neg\ R(x))\]
    \begin{center}
        \begin{tikzpicture}
            [level 1/.style = {sibling distance = 3cm, level distance = 1.2cm}]

            \node {$\exists x\ \neg\ R(x)\ \scriptstyle(EE)$ $\checkmark$}
            child {node {$\neg R(a)$}
                child {node {$\forall x\ P(x)\ \scriptstyle(EU)$}
                    child {node {$P(b)$}
                        child {node {$\forall y\ (Q(y)\rightarrow R(y))\ \scriptstyle(EU)$}
                            child {node {$\neg Q(b)$}
                                child {node {$\forall x\ (P(x)\rightarrow Q(x))\ \scriptstyle(EU)$}
                                    child {node {$\neg P(b)$}
                                    edge from parent node [below, xshift=-21px, yshift=-24px] {$\ast$}}
                                    child {node {$Q(b)$}
                                    edge from parent node [below, xshift=21px, yshift=-24px] {$\ast$}}
                                }
                            }
                            child {node {$R(b)$}
                            edge from parent node [below, xshift=21px, yshift=-24px] {$\ast$}}
                        }
                    }
                }
            };
        \end{tikzpicture}
    \end{center}

    El argumento es válido ya que por reducción al absurdo es una contradicción
    \subsection{Demostración por inducción}

    Para decir que una proposición es verdadera usando el \textbf{principio de inducción} debemos comprobar que siendo P una propiedad definida sobre los números naturales tal que:
    \begin{itemize}
        \item \textbf{(Base inductiva)} P(1) es cierta y,
        \item \textbf{(Paso inductivo)} $\forall k\ (P(k)\models P(k+1))$
    \end{itemize}

    Entonces $\forall n\ P(n)$ es una proposición verdadera (en caso de números naturalesm se cumpliría para cualquier número natural)\\

    En otras ocasiones debemos usar el \textbf{principio de inducción fuerte} que dice:

    Sea P una propiedad definida sobre los números naturales tal que:

    \begin{itemize}
        \item \textbf{(Base inductiva)} P(1) es cierta y,
        \item \textbf{(Paso inductivo completo)} $\{P(1), P(2),...,P(k)\}\models P(k+1)$
    \end{itemize}

    \section{Álgebras de Boole}

    Un Álgebra de Boole es un conjunto A con dos operaciones binarias $+$ y $\cdot$  y una operación unaria ($\bar{\ \ \ }$)\\

    El álgebra de Boole más sencilla es la formada por el conjunto $A=\{0,1\}$ con operaciones:

    \begin{multicols}{3}
       \begin{center}
        \begin{tabular}{|cc|c|}
            \hline
            $x$ & $y$ & $x+y$ \\ \hline
            0 & 0 & 0   \\
            0 & 1 & 1   \\
            1 & 1 & 1   \\
            1 & 0 & 1   \\ \hline
            \end{tabular}
       \end{center} 
       \columnbreak
       \begin{center}
        \begin{tabular}{|cc|c|}
            \hline
            $x$ & $y$ & $x\cdot y$ \\ \hline
            0 & 0 & 0   \\
            0 & 1 & 0   \\
            1 & 1 & 1   \\
            1 & 0 & 0   \\ \hline
            \end{tabular}
       \end{center}
       \columnbreak
       \begin{center}
        \begin{tabular}{cc}
                                      &                                     \\ \hline
            \multicolumn{1}{|c|}{$x$} & \multicolumn{1}{c|}{$\overline{x}$} \\ \hline
            \multicolumn{1}{|c|}{0}   & \multicolumn{1}{c|}{1}              \\
            \multicolumn{1}{|c|}{1}   & \multicolumn{1}{c|}{1}              \\ \hline
                                      &                                    
        \end{tabular}
       \end{center}
    \end{multicols}

    Las propiedades son equivalentes a las de la lógica proposicional:
    \begin{multicols}{2}
        \begin{center}
            \vspace*{\fill}
            \begin{tabular}{|c|c|}
                \hline
                Boole     & Lógica   \\ \hline
                $+$       & $\vee$   \\
                $\cdot$   & $\wedge$ \\
                $\bar{\ }$& $\neg$   \\
                0         & $\perp$  \\
                1         & $\top$    \\ \hline
            \end{tabular}
            \vspace*{\fill}
        \end{center}
        \columnbreak
        \begin{center}
            \setlength{\tabcolsep}{15pt}
            \renewcommand{\arraystretch}{1.2}
            \begin{tabular}{|cl|}
                \hline
                \multicolumn{2}{|c|}{Propiedades álgebra Boole} \\ \hline
                \multicolumn{1}{|c|}{Idempotencia}       & $ \begin{array}{lcl} a+a=a \\a\cdot a = a \end{array}$ \\ \hline
                \multicolumn{1}{|c|}{Acotación}          & $ \begin{array}{lcl} a+1=1 \\a\cdot 0 = 0 \end{array}$ \\ \hline
                \multicolumn{1}{|c|}{Absorción}          & $ \begin{array}{lcl} a+(a\cdot b)=a \\a\cdot (a+b)=a \end{array}$ \\ \hline
                \multicolumn{1}{|c|}{Elemento único}     & $ \begin{array}{lcl} a+b=1 \text{ y}\\ a\cdot b = 0 \text{ entonces}\\ b=\overline{a} \end{array}$ \\ \hline
                \multicolumn{1}{|c|}{Morgan}             & $ \begin{array}{lcl} \overline{a+b}=\overline{a}\cdot \overline{b} \\ \overline{a\cdot b}=\overline{a}+\overline{b} \end{array}$ \\ \hline
                \multicolumn{1}{|c|}{Involución}         & $ \overline{(\overline{a})}=a$ \\ \hline
                \multicolumn{1}{|c|}{}                   & $ \begin{array}{lcl} a+(\overline{a}\cdot b)=a+b \\ a\cdot(\overline{a}+b)=a\cdot b \end{array}$ \\ \hline
            \end{tabular}
        \end{center}
    \end{multicols}

    \subsection{Funciones de Boole}
    Dada el álgebra de Boole $A=\{0,1\}$ se llama $x$ variable booleana si toma valores de $A$.
    Existe una jerarquía de operaciones que permite suprimir paréntesis:
    \begin{center}
        1. Complemento $\bar{\ } \qquad \qquad$ 2. Producto $\cdot \qquad \qquad$ 3. Suma $+$
    \end{center}

    Una función $f$ de n variables sobre un álgebra de Boole $A$ es una aplicación:
    \[f: A_1\times ... \times A_n\rightarrow A\]
    Por lo que si $A=\{0,1\}$ hay $2^n$ posibles combinaciones de entradas.\\

    Una función de Boole puede definirse medianbte expresiones del álgebra de Boole o con una tabla de valores.\\

    $\Rightarrow$ \textbf{Ejemplo:} $f(a,b)=\overline{a}+\overline{a}\cdot b$
    \begin{multicols}{2}
        \[
           \begin{array}{l}
                f(0,0) = 1+1\cdot 0 = 1\\
                f(0,1) = 1+1\cdot 1 = 1\\
                f(1,0) = 0+0\cdot 0 = 0\\
                f(1,1) = 0+0\cdot 1 = 0
           \end{array} 
        \]
        \columnbreak
        \begin{center}
            \begin{tabular}{|cc|c|c|}
                \hline
                $a$ & $b$ & $\overline{a}$ & $f(a,b)$ \\ \hline
                0   & 0   & 1        & 1        \\ \hline
                0   & 1   & 1        & 1        \\ \hline
                1   & 0   & 0        & 0        \\ \hline
                1   & 1   & 0        & 0        \\ \hline
            \end{tabular}
        \end{center}
    \end{multicols}

    Dos funciones se dicen iguales si sus tablas de valores son iguales. También se puede saber simplificando la expresión de $f$ aplicando las propiedades
    \[f(a,b)=\overline{a}+\overline{a}\cdot b=\overline{a}\quad \text{(por absorción)}\]

    \vspace{8px}
    Un literal es una variable booleana o el complemento de una variable booleana. Escribiremos una función booleana usando sumas o productos de literales a partir de su tabla de valores, señalando los productos para los que f(a,b)=1 y las sumas para las que f(a,b)=0.\\

    Se verifica que cualquien función booleana puede expresarse en forma de suma de productos de literales \textbf{(Forma Normal Disyuntiva, FND)} o en forma de producto de sumas de literales \textbf{(Forma Normal Conjuntiva, FNC)}.\\

    Por ejemplo la tabla de valores del ejemplo anterior:\\

    \begin{center}
        \begin{tabular}{r|cc|c|l}
            \cline{2-4}
                                                        & $a$ & $b$ & $f(a,b)$ &                                                   \\ \cline{2-4}
                                                        & 0   & 0   & 1        & $\longrightarrow\ \overline{a}\cdot \overline{b}$ \\ \cline{2-4}
                                                        & 0   & 1   & 1        & $\longrightarrow\ \overline{a}\cdot b$            \\ \cline{2-4}
            $\overline{a}+b\ \longleftarrow$            & 1   & 0   & 0        &                                                   \\ \cline{2-4}
            $\overline{a}+\overline{b}\ \longleftarrow$ & 1   & 1   & 0        &                                                   \\ \cline{2-4}
        \end{tabular}
    \end{center}
    \[\text{FND}\ f(a,b)= (\overline{a}\cdot\overline{b})+(\overline{a}\cdot b)\qquad \text{o} \qquad \text{FNC}\ f(a,b)=(\overline{a}+b)\cdot(\overline{a}+\overline{b})\]
    \vspace{15px}
     
   \textbf{ En resumen:}
    \begin{center}
        \setlength{\tabcolsep}{10pt}
        \renewcommand{\arraystretch}{1.2}
        \begin{tabular}{|c|c|c|c|}
            \hline
                & f     & obtención         & convenio \\ \hline
            FND & $f=1$ & $(\cdot)+(\cdot)$ & $1=x\ 0=\overline{x}$      \\ \hline
            FNC & $f=0$ & $(+)\cdot(+)$     & $0=x\ 1=\overline{x}$      \\ \hline
            \end{tabular}
    \end{center}

    \subsection{Minimización de funciones}
    Minimizar una función es obtener la expresión más simple posible para dicha función.\\

    Los \textbf{diagramas o mapas de Karnaugh} es una presentación alternativa de los valores de la función. El número de casillas depende del número de variables $(2^n)$.\\

    Diremos que dos celdas son \textbf{adyacentes} si el producto o suma que representan difieren en un solo literal.\\
    
    El producto de literales obtenido de combinar dos celdas es un \textbf{implicante} y es un \textbf{implicante primo} si no está contenido en ningún bloque mayor. Es un \textbf{implicante primo esencial} si cubre una casilla de forma exclusiva.

    \begin{enumerate}
        \item Colocamos un 1 (FND) o un 0 (FNC) en la celda correspondiente
        \item Agrupamos las celdas adyacentes (identificamos los bloques de mayor tamaño)
        \item Eliminamos la varibale que es diferente
    \end{enumerate}

    $\Rightarrow$ \textbf{Ejemplo}: Diagrama de la función $f(x,y)=\textcolor{Purple}{x\overline{y}}+\textcolor{Turquoise}{\overline{x}y}+\textcolor{Salmon}{\overline{x}\overline{y}}$

    \begin{multicols}{3}
        \begin{center}
            \vspace*{\fill}
            \begin{tabular}{cc|c}
                $x$ & $y$ & $f$ \\ \hline
                0   & 0   & 1   \\
                0   & 1   & 1   \\
                1   & 0   & 1   \\
                1   & 1   & 0   \\
            \end{tabular}
            \vspace*{\fill}
        \end{center}
        \columnbreak
        \begin{center}
            \begin{kvmap}
                \begin{kvmatrix}{y,x}
                    \textcolor{Salmon}{1} & \textcolor{Turquoise}{1} \\
                    \textcolor{Purple}{1} &   \\
                \end{kvmatrix}
                %      x1,y1,x2,y2
                \bundle{0}{0}{1}{0}
                \bundle{0}{0}{0}{1}
            \end{kvmap}
        \end{center}
        \columnbreak
        \vspace*{\fill}
        \begin{tikzpicture} [remember picture,overlay]
            \draw (-1.55,0.5) [to-] to (1,0.5) node[right]{$\overline{x}\overline{y}+\overline{x}y=\overline{x}$};
            \draw (-2.56,-0.6) [to-] to (1,-0.6) node[right]{$\overline{x}\overline{y}+x\overline{y}=\overline{y}$};
        \end{tikzpicture}
        \vspace*{\fill}
    \end{multicols}

    El resultado sería: $\qquad f(x,y)=x\overline{y}+\overline{x}y+\overline{x}\overline{y}=\overline{x}+\overline{y}$\\

    Para funciones de más variables el diagrama puede verse como un cilindro o un toroide para buscar adyacentes.\\

    $\Rightarrow$ \textbf{Ejercicio 26 ii}: Haya un mapa de Karnaugh, una FND y simplifícala para:

    $f(x,y,z)=(\overline{x}+\overline{y}+\overline{z})\cdot z + \overline{(\overline{y}+z)} = \overline{x}\cdot z +\overline{y}\cdot z +\overline{z}\cdot z + (y\cdot \overline{z}) = \overline{x}\cdot z +\overline{y}\cdot z + y\cdot \overline{z}$
    \[
        \begin{array}{l}
            \textcolor{Purple}{\overline{x}\cdot z} + \textcolor{Salmon}{\overline{y}\cdot z} + \textcolor{Turquoise}{y\cdot \overline{z}}\\
            0\cdot 1 +0\cdot 1 + 1\cdot 0
        \end{array}
    \]
    
    \begin{multicols}{2}
        \begin{center}
            \begin{kvmap}
                \begin{kvmatrix}{y,z,x}
                     & \textcolor{Purple}{1} & \textcolor{Purple}{1} & \textcolor{Turquoise}{1} \\
                     & \textcolor{Salmon}{1} &                       & \textcolor{Turquoise}{1} \\
                \end{kvmatrix}
                %      x1,y1,x2,y2
                \bundle{1}{0}{1}{1}
                \bundle{1}{0}{2}{0}
                \bundle{3}{0}{3}{1}
            \end{kvmap}
        \end{center}
        \columnbreak
        \vspace*{\fill}
        \begin{tikzpicture} [remember picture,overlay]
            \draw (-2.9,0.72)[to-, bend left=25] to (2,1) node[right]{$\bar{x}\bar{y}z+\bar{x}yz=\bar{x}z$};
            \draw (-1.9,0.2) [to-] to (2,0.2) node[right]{$\bar{x}y\bar{z}+xy\bar{y}=y\bar{z}$};
            \draw (-3.9,-0.6) [to-, bend left=-20]to (2,-0.6) node[right]{$\bar{x}\bar{y}z+x\bar{y}z=\bar{y}z$};
        \end{tikzpicture}
        \vspace*{\fill}
    \end{multicols}
    \vspace{6px}
    El resultado sería: $\qquad f(x,y,z)=\bar{x}z+y\bar{z}+\bar{y}z$

\end{document}